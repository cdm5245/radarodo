/********************************COPYRIGHT CHRIS D. MONACO, 2019**********************************/
#include <iomanip>

#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

#include <radarodo/visual_debugger.h>

using namespace VisualDebugger;

void VisualDebugger::GenerateColormapImage(const cv::Mat& image, cv::Mat& colormap_image) {
  image.convertTo(colormap_image,
                  CV_8UC1,
                  static_cast<double>(std::numeric_limits<uchar>::max()) /
                    static_cast<double>(std::numeric_limits<ushort>::max()));
  cv::applyColorMap(colormap_image, colormap_image, cv::COLORMAP_JET);
}

void VisualDebugger::WriteImageToDisk(const std::string& package_path,
                                      const uint msg_seq,
                                      const cv::Mat& image,
                                      const std::string& prefix) {
  std::ostringstream image_filename;
  image_filename << std::setfill('0') << std::setw(10) << msg_seq;

  cv::imwrite(package_path + "/debugging_images/"
              + prefix + "/" + prefix + image_filename.str() + ".png", image);
}

void VisualDebugger::RecordDebuggingImages(
    const std::string& package_path,
    const uint msg_seq,
    const cv::Mat& blurred_polar_image,
    const Eigen::Vector3d& sensor_motion_estimate,
    const Eigen::Array<double, Eigen::Dynamic, 3>& extracted_point_data,
    const double delta_t,
    const double gps_yaw_rate,
    const double radians_per_pixel) {
  cv::Mat pre_rot_image, post_rot_image, gps_rot_image, display_image;
  GenerateColormapImage(blurred_polar_image, pre_rot_image);
  pre_rot_image.copyTo(post_rot_image);
  pre_rot_image.copyTo(gps_rot_image);

  const double radarodo_u_pixel_offset = sensor_motion_estimate.z() * delta_t / radians_per_pixel;
  const double gps_u_pixel_offset = gps_yaw_rate * delta_t / radians_per_pixel;

  AnnotateImageWithPoints(extracted_point_data, 0, pre_rot_image);
  AnnotateImageWithPoints(extracted_point_data, radarodo_u_pixel_offset, post_rot_image);
  AnnotateImageWithPoints(extracted_point_data, gps_u_pixel_offset, gps_rot_image);

  const double initial_error =
    CalculateError(extracted_point_data, 0, blurred_polar_image, false);
  const double weighted_initial_error =
    CalculateError(extracted_point_data, 0, blurred_polar_image, true);
  const double radarodo_error =
    CalculateError(extracted_point_data, radarodo_u_pixel_offset, blurred_polar_image, false);
  const double weighted_radarodo_error =
    CalculateError(extracted_point_data, radarodo_u_pixel_offset, blurred_polar_image, true);
  const double gps_error =
    CalculateError(extracted_point_data, gps_u_pixel_offset, blurred_polar_image, false);
  const double weighted_gps_error =
    CalculateError(extracted_point_data, gps_u_pixel_offset, blurred_polar_image, true);

  AnnotateImageWithInfo(initial_error, weighted_initial_error, radarodo_error,
                        weighted_radarodo_error, gps_error, weighted_gps_error,
                        sensor_motion_estimate[2], gps_yaw_rate, delta_t, radians_per_pixel,
                        pre_rot_image, post_rot_image, gps_rot_image);

  cv::hconcat(pre_rot_image, post_rot_image, display_image);
  cv::hconcat(display_image, gps_rot_image, display_image);

  WriteImageToDisk(package_path, msg_seq, display_image, "debugging");
}

void VisualDebugger::AnnotateImageWithPoints(
    const Eigen::Array<double, Eigen::Dynamic, 3>& extracted_point_data,
    const double u_pixel_offset,
    cv::Mat& polar_image) {
  for (uint row_idx = 0; row_idx < extracted_point_data.rows(); ++row_idx) {
    cv::circle(polar_image,
               cv::Point(extracted_point_data(row_idx, 0) + u_pixel_offset,
                         extracted_point_data(row_idx, 1)),
                         1,
                         cv::Vec3b(255, 0, 255),
                         -1);
  }
}

double VisualDebugger::CalculateError(
    const Eigen::Array<double, Eigen::Dynamic, 3>& extracted_point_data,
    const double u_pixel_offset,
    const cv::Mat& polar_image,
    const bool weighted) {
  double point_error;
  double error = 0;
  for (uint row_idx = 0; row_idx < extracted_point_data.rows(); ++row_idx) {
    point_error = 1.0
                  - static_cast<double>(polar_image.at<ushort>(
                      cv::Point(extracted_point_data(row_idx, 0)
                                + u_pixel_offset,
                                extracted_point_data(row_idx, 1)))) /
                      static_cast<double>(std::numeric_limits<ushort>::max());
    error = weighted ? error + extracted_point_data(row_idx, 2) * point_error :
                       error + point_error;
  }

  error /= weighted ? extracted_point_data.rightCols<1>().sum() :
                      extracted_point_data.rows();

  return error;
}

void VisualDebugger::AnnotateImageWithInfo(
    const double initial_error,
    const double weighted_initial_error,
    const double radarodo_error,
    const double weighted_radarodo_error,
    const double gps_error,
    const double weighted_gps_error,
    const double radarodo_yaw_rate,
    const double gps_yaw_rate,
    const double delta_t,
    const double radians_per_pixel,
    cv::Mat& pre_rot_image,
    cv::Mat& post_rot_image,
    cv::Mat& gps_rot_image) {
  cv::putText(pre_rot_image,
              "Initial Avg. Error: " + std::to_string(initial_error),
              cv::Point(0, 50),
              cv::FONT_HERSHEY_PLAIN,
              1,
              cv::Scalar(255, 255, 255));
  cv::putText(pre_rot_image,
              "Initial Avg. Weighted Error: " + std::to_string(weighted_initial_error),
              cv::Point(0, 100),
              cv::FONT_HERSHEY_PLAIN,
              1,
              cv::Scalar(255, 255, 255));

  cv::putText(post_rot_image,
              "RADARODO Yaw Rate [deg/s]: " + std::to_string(radarodo_yaw_rate * 180.0 / M_PI),
              cv::Point(0, 50),
              cv::FONT_HERSHEY_PLAIN,
              1,
              cv::Scalar(255, 255, 255));
  cv::putText(post_rot_image,
              "RADARODO Pixel Shift: " + std::to_string(radarodo_yaw_rate * delta_t /
                                                          radians_per_pixel),
              cv::Point(0, 100),
              cv::FONT_HERSHEY_PLAIN,
              1,
              cv::Scalar(255, 255, 255));
  cv::putText(post_rot_image,
              "RADARODO Avg. Error: " + std::to_string(radarodo_error),
              cv::Point(0, 150), cv::FONT_HERSHEY_PLAIN,
              1,
              cv::Scalar(255, 255, 255));
  cv::putText(post_rot_image,
              "RADARODO Avg. Weighted Error: " + std::to_string(weighted_radarodo_error),
              cv::Point(0, 200), cv::FONT_HERSHEY_PLAIN,
              1,
              cv::Scalar(255, 255, 255));

  cv::putText(gps_rot_image,
              "GPS Yaw Rate [deg/s]: " + std::to_string(gps_yaw_rate * 180.0 / M_PI),
              cv::Point(0, 50),
              cv::FONT_HERSHEY_PLAIN,
              1,
              cv::Scalar(255, 255, 255));
  cv::putText(gps_rot_image,
              "GPS Pixel Shift: " + std::to_string(gps_yaw_rate * delta_t / radians_per_pixel),
              cv::Point(0, 100),
              cv::FONT_HERSHEY_PLAIN,
              1,
              cv::Scalar(255, 255, 255));
  cv::putText(gps_rot_image,
              "GPS Avg. Error: " + std::to_string(gps_error),
              cv::Point(0, 150),
              cv::FONT_HERSHEY_PLAIN,
              1,
              cv::Scalar(255, 255, 255));
  cv::putText(gps_rot_image,
              "GPS Avg. Weighted Error: " + std::to_string(weighted_gps_error),
              cv::Point(0, 200),
              cv::FONT_HERSHEY_PLAIN,
              1,
              cv::Scalar(255, 255, 255));

  const double pixel_shift_error = (radarodo_yaw_rate - gps_yaw_rate) * delta_t /
                                     radians_per_pixel;
  cv::Scalar text_color = (std::abs(pixel_shift_error) > 2) ?
                            cv::Scalar(0, 0, 255) : cv::Scalar(255, 255, 255);
  cv::putText(post_rot_image,
              "Pixel Shift Error: " + std::to_string(pixel_shift_error),
              cv::Point(0, 250),
              cv::FONT_HERSHEY_PLAIN,
              1,
              text_color);
}

void VisualDebugger::RecordPolarAndCartesianImages(
    const std::string& package_path,
    const uint msg_seq,
    const cv::Mat& polar_image,
    const cv::Mat& cartesian_image,
    const cv::Mat& polar_non_field_of_view_mask,
    const cv::Mat& cartesian_non_field_of_view_mask) {
  cv::Mat polar_colormap_image;
  GenerateColormapImage(polar_image, polar_colormap_image);

  cv::Mat cartesian_colormap_image;
  GenerateColormapImage(cartesian_image, cartesian_colormap_image);

  polar_colormap_image.setTo(cv::Vec3b(255, 255, 255),
                            polar_non_field_of_view_mask);
  cartesian_colormap_image.setTo(cv::Vec3b(255, 255, 255),
                                 cartesian_non_field_of_view_mask);

  WriteImageToDisk(package_path, msg_seq, polar_colormap_image, "polar");
  WriteImageToDisk(package_path, msg_seq, cartesian_colormap_image, "cartesian");
}

void VisualDebugger::RecordExtractedPointImages(
    const std::string& package_path,
    const uint msg_seq,
    const cv::Mat& polar_image,
    const Eigen::Array<double, Eigen::Dynamic, 3>& extracted_point_data,
    const cv::Mat& polar_non_field_of_view_mask,
    const double u_pixel_offset,
    const std::string& prefix) {
  cv::Mat polar_colormap_image;
  GenerateColormapImage(polar_image, polar_colormap_image);

  AnnotateImageWithPoints(extracted_point_data, u_pixel_offset, polar_colormap_image);
  polar_colormap_image.setTo(cv::Vec3b(255, 255, 255),
                            polar_non_field_of_view_mask);

  WriteImageToDisk(package_path, msg_seq, polar_colormap_image, prefix);
}
