/********************************COPYRIGHT CHRIS D. MONACO, 2019**********************************/
#include <g2o/core/block_solver.h>
#include <g2o/solvers/csparse/linear_solver_csparse.h>
#include <g2o/core/optimization_algorithm_levenberg.h>
#include <g2o/types/slam2d/vertex_point_xy.h>
#include <g2o/types/slam2d/edge_xy_prior.h>

#include <radarodo/trans_vel_estimator.h>
#include <radarodo/doppler_edge.h>

typedef g2o::BlockSolver< g2o::BlockSolverTraits<-1, -1> > SlamBlockSolver;
typedef g2o::LinearSolverCSparse<SlamBlockSolver::PoseMatrixType> SlamLinearSolver;

static constexpr uint kSensorPoseVertexId = 0;

TransVelEstimator::TransVelEstimator(const double ransac_iterations,
                                     const double range_rate_inlier_threshold,
                                     const double target_range_rate_sigma,
                                     const double target_theta_sigma,
                                     const uint max_num_optimization_iterations,
                                     const bool verbose)
  : ransac_iterations_(ransac_iterations),
    range_rate_inlier_threshold_(range_rate_inlier_threshold),
    max_num_optimization_iterations_(max_num_optimization_iterations),
    measurement_covariance_matrix_(Eigen::DiagonalMatrix<double, 2>(
      target_range_rate_sigma * target_range_rate_sigma,
      target_theta_sigma * target_theta_sigma)) {
  optimizer_pose_graph_.setVerbose(verbose);
  Eigen::initParallel();

  // Initialize the optimizer pose graph

  auto linear_solver_unique_ptr = g2o::make_unique<SlamLinearSolver>();
  linear_solver_unique_ptr->setBlockOrdering(false);

  auto slam_block_solver_unique_ptr =
    g2o::make_unique<SlamBlockSolver>(std::move(linear_solver_unique_ptr));
  auto solver_levenberg_unique_ptr =
    g2o::make_unique<g2o::OptimizationAlgorithmLevenberg>(std::move(slam_block_solver_unique_ptr));

  optimizer_pose_graph_.setAlgorithm(std::move(solver_levenberg_unique_ptr).get());
  solver_levenberg_unique_ptr.release();
}

bool TransVelEstimator::Estimate(std::vector<RadarDetection>& valid_radar_detections,
                                 Eigen::Vector3d& sensor_motion_estimate,
                                 Eigen::Matrix<double, 3, 3>& sensor_motion_covariance_matrix) {
  // Generate the matrices that describe the linear system relating Doppler measurements to the
  // sensor's translational velocities. Also generate associated vectors. Each row is a RADAR
  // detected point target. The columns of A are the linear coefficients that relate v_x and v_y,
  // respectively, to the range rate (in b). A is based on the azimuthal angles of the detected
  // RADAR targets.
  Eigen::Matrix<double, Eigen::Dynamic, 2> A;
  Eigen::Matrix<double, Eigen::Dynamic, 1> b;
  Eigen::Matrix<double, Eigen::Dynamic, 1> ranges;
  Eigen::Matrix<double, Eigen::Dynamic, 1> thetas;
  GenerateMatrices(valid_radar_detections, A, b, ranges, thetas);

  // Extract (static) inlier targets via RANSAC-scheme. Return if insufficient number of inliers.
  Eigen::Array<uint, Eigen::Dynamic, 1> inlier_flags;
  const uint num_inliers = ExtractInliers(A, b, inlier_flags, valid_radar_detections);
  if (num_inliers < 3) return false;

  // Using inlier flags, create matrices/vectors that only include the detected inliers.
  Eigen::Matrix<double, Eigen::Dynamic, 2> A_inliers;
  Eigen::Matrix<double, Eigen::Dynamic, 1> b_inliers;
  Eigen::Matrix<double, Eigen::Dynamic, 1> range_inliers;
  Eigen::Matrix<double, Eigen::Dynamic, 1> theta_inliers;
  GenerateInlierMatrices(A, b, ranges, thetas, inlier_flags, num_inliers,
                         A_inliers, b_inliers, range_inliers, theta_inliers);

  // Calculate the least squares solution of the sensor's translational velocities. Then, generate
  // a pose graph for a non-linear optimization that refines this solution by using the least
  // squares solution as the initial estimate.
  GeneratePoseGraph(A_inliers,
                    b_inliers,
                    theta_inliers,
                    EstimateLeastSquaresSolution(A_inliers, b_inliers));

  // Optimize the pose graph via gradient descent to refine the trans. vel. estimate
  optimizer_pose_graph_.initializeOptimization();
  optimizer_pose_graph_.optimize(max_num_optimization_iterations_);

  // Get the pose estimate from the optimizer pose graph and then calculate its
  // expected covariance matrix.
  sensor_motion_estimate.topRows<2>() = PoseEstimate(kSensorPoseVertexId);
  CalculateCovarianceMatrix(A_inliers, range_inliers, sensor_motion_covariance_matrix);

  return true;
}

void TransVelEstimator::GenerateMatrices(
    const std::vector<RadarDetection>& valid_radar_detections,
    Eigen::Matrix<double, Eigen::Dynamic, 2>& A,
    Eigen::Matrix<double, Eigen::Dynamic, 1>& b,
    Eigen::Matrix<double, Eigen::Dynamic, 1>& ranges,
    Eigen::Matrix<double, Eigen::Dynamic, 1>& thetas) {
  b = Eigen::Matrix<double, Eigen::Dynamic, 1>(valid_radar_detections.size(), 1);
  ranges = Eigen::Matrix<double, Eigen::Dynamic, 1>(valid_radar_detections.size(), 1);
  thetas = Eigen::Matrix<double, Eigen::Dynamic, 1>(valid_radar_detections.size(), 1);

  for (uint target_idx = 0; target_idx < valid_radar_detections.size(); ++target_idx) {
    b[target_idx] = valid_radar_detections[target_idx].range_rate;
    ranges[target_idx] = valid_radar_detections[target_idx].range;
    thetas[target_idx] = valid_radar_detections[target_idx].theta;
  }

  // Each row of A corresponds to a valid radar detection. The columns of A are the
  // linear coefficients that relate v_x and v_y, respectively, to the range rate (in b).
  // A is based on the azimuthal angles of the detected RADAR targets.
  A = Eigen::Matrix<double, Eigen::Dynamic, 2>(valid_radar_detections.size(), 2);
  A << - thetas.array().cos(), - thetas.array().sin();
}

uint TransVelEstimator::ExtractInliers(
    const Eigen::Matrix<double, Eigen::Dynamic, 2>& A,
    const Eigen::Matrix<double, Eigen::Dynamic, 1>& b,
    Eigen::Array<uint, Eigen::Dynamic, 1>& inlier_flags,
    std::vector<RadarDetection>& valid_radar_detections) {
  // Random indices are selected for each RANSAC iteration. Since two targets are the minimum
  // for a trans. vel. estimate, two random RADAR targets are choosen for each RANSAC iteration.
  // This corresponds to the two columns of random_indices. The random integers are generated
  // but then they are transformed to correspond to the possible range of plausible row indices
  // for A and b.
  Eigen::Array<int, Eigen::Dynamic, 2> random_indices =
    Eigen::Array<int, Eigen::Dynamic, 2>::Random(ransac_iterations_, 2).cwiseAbs();
  random_indices -= A.rows() * (random_indices / A.rows());

  Eigen::Matrix<double, 2, 2> A_ransac_it;
  Eigen::Matrix<double, 2, 1> b_ransac_it;
  Eigen::Vector2d ransac_iteration_trans_velocity_estimate;
  Eigen::Array<double, Eigen::Dynamic, 1> ransac_iteration_residuals;
  Eigen::Array<bool, Eigen::Dynamic, 1> ransac_iteration_inliers;
  uint num_best_inliers = 0;
  for (uint ransac_iteration = 0;
      ransac_iteration < ransac_iterations_;
      ++ransac_iteration) {
    // Skip this iteration if the same target was selected as the first and second target
    if (random_indices(ransac_iteration, 0) == random_indices(ransac_iteration, 1)) continue;

    // Create a 2 row "RANSAC" A and b matrices based on the randomly selected target
    A_ransac_it << A.row(random_indices(ransac_iteration, 0)),
                   A.row(random_indices(ransac_iteration, 1));
    b_ransac_it << b(random_indices(ransac_iteration, 0)),
                   b(random_indices(ransac_iteration, 1));

    // Calculate the translational velocity estimate and then, the range rate residuals for all
    // targets. Flag the targets whose residuals are below the inlier threshold as static
    // inlier candidates.
    ransac_iteration_trans_velocity_estimate =
      (A_ransac_it.transpose() * A_ransac_it).ldlt().solve(A_ransac_it.transpose() * b_ransac_it);
    ransac_iteration_residuals = (A * ransac_iteration_trans_velocity_estimate - b).cwiseAbs();
    ransac_iteration_inliers = ransac_iteration_residuals < range_rate_inlier_threshold_;

    // In this simplistic scheme, more inliers implies a better solution. Therefore, if the number
    // of inliers for this iterations solution exceeds the highest previously observed number of
    // inliers, this iteration's solution is the new best solution. Therefore, store its inlier
    // flags and number of inliers for comparison with future iterations.
    if (ransac_iteration_inliers.count() > num_best_inliers) {
      // Note that an inlier is marked with the integer 1 once casted. This is necessary for
      // future inlier extraction.
      inlier_flags = ransac_iteration_inliers.cast<uint>();
      num_best_inliers = ransac_iteration_inliers.count();
    }
  }

  // Once the best solution has been found, mark the coresponding RadarDetection objects
  // as static according to the solution's inlier flags
  for (uint target_idx = 0; target_idx < valid_radar_detections.size(); ++target_idx) {
    if (inlier_flags[target_idx]) valid_radar_detections[target_idx].MarkStatic();
  }

  return num_best_inliers;
}

void TransVelEstimator::GenerateInlierMatrices(
    const Eigen::Matrix<double, Eigen::Dynamic, 2>& A,
    const Eigen::Matrix<double, Eigen::Dynamic, 1>& b,
    const Eigen::Matrix<double, Eigen::Dynamic, 1>& ranges,
    const Eigen::Matrix<double, Eigen::Dynamic, 1>& thetas,
    const Eigen::Array<uint, Eigen::Dynamic, 1>& inlier_flags,
    const uint num_inliers,
    Eigen::Matrix<double, Eigen::Dynamic, 2>& A_inliers,
    Eigen::Matrix<double, Eigen::Dynamic, 1>& b_inliers,
    Eigen::Matrix<double, Eigen::Dynamic, 1>& range_inliers,
    Eigen::Matrix<double, Eigen::Dynamic, 1>& theta_inliers) {
  A_inliers = Eigen::Matrix<double, Eigen::Dynamic, 2>(num_inliers, 2);
  b_inliers = Eigen::Matrix<double, Eigen::Dynamic, 1>(num_inliers, 1);
  range_inliers = Eigen::Matrix<double, Eigen::Dynamic, 1>(num_inliers, 1);
  theta_inliers = Eigen::Matrix<double, Eigen::Dynamic, 1>(num_inliers, 1);

  // Create a vector, inliers_original_row_idxs, that indicates the row index
  // for each inlier target in a new matrix that only includes inliers. This
  // is done by creating a vector that computes the partial sum of the inlier
  // flags. Since inliers are marked with the number 1, the partial sum only
  // increases in locations with an inlier. Thus, after subtraction by 1 to
  // account for zero-based indexing, each inlier should be placed at the
  // new row index stored here when looked up by their corresponding row.
  Eigen::Array<uint, Eigen::Dynamic, 1> inliers_original_row_idxs(A.rows());
  std::partial_sum(inlier_flags.data(),
                   inlier_flags.data() + A.rows(),
                   inliers_original_row_idxs.data());
  inliers_original_row_idxs -= 1;

  // For all targets (rows in A), check to see if a target is an inlier. If it is,
  // lookup its new row index from inliers_original_row_idxs. Then place its data
  // in that row in the new matrices/vectors that only include inliers.
  uint inlier_idx;
  for (uint target_idx = 0;
      target_idx < A.rows();
      ++target_idx) {
    if (!inlier_flags(target_idx)) continue;
    inlier_idx = inliers_original_row_idxs(target_idx);
    A_inliers.row(inlier_idx) = A.row(target_idx);
    b_inliers[inlier_idx] = b[target_idx];
    range_inliers[inlier_idx] = ranges[target_idx];
    theta_inliers[inlier_idx] = thetas[target_idx];
  }
}

void TransVelEstimator::GeneratePoseGraph(
    const Eigen::Matrix<double, Eigen::Dynamic, 2>& A_inliers,
    const Eigen::Matrix<double, Eigen::Dynamic, 1>& b_inliers,
    const Eigen::Matrix<double, Eigen::Dynamic, 1>& theta_inliers,
    const Eigen::Vector2d& sensor_trans_motion_estimate) {
  // Generates skeleton framework of pose graph by clearing graph, constructing pose node,
  // and attaching a prior edge
  GenerateSkeletonFramework(sensor_trans_motion_estimate);

  // For each inlier RADAR target detection, construct an edge constraint and attach it to
  // sensor pose node that represents its translational velocities.
  for (uint row_idx = 0; row_idx < A_inliers.rows(); ++row_idx) {
    auto prior_edge_unique_ptr = g2o::make_unique<Radarodo::DopplerEdge>(
      b_inliers[row_idx],
      theta_inliers[row_idx],
      // the negative is taken to negate -cos(theta) in A_inliers
      - A_inliers(row_idx, 0),
      // the negative is taken to negate -sin(theta) in A_inliers
      - A_inliers(row_idx, 1),
      measurement_covariance_matrix_,
      optimizer_pose_graph_.vertex(kSensorPoseVertexId));
    optimizer_pose_graph_.addEdge(std::move(prior_edge_unique_ptr).get());
    prior_edge_unique_ptr.release();
  }
}

void TransVelEstimator::GenerateSkeletonFramework(
    const Eigen::Vector2d& sensor_trans_motion_estimate) {
  // Clear optimizer pose graph from previous iteration
  optimizer_pose_graph_.clear();

  // Create sensor pose vertex that represents the sensor's translational velocities
  auto sensor_pose_vertex_unique_ptr = g2o::make_unique<g2o::VertexPointXY>();
  sensor_pose_vertex_unique_ptr->setEstimate(sensor_trans_motion_estimate);
  sensor_pose_vertex_unique_ptr->setId(kSensorPoseVertexId);
  optimizer_pose_graph_.addVertex(std::move(sensor_pose_vertex_unique_ptr).get());
  sensor_pose_vertex_unique_ptr.release();

  // Create the prior edge for the sensor pose vertex but give it a negligible weight (information)
  auto prior_edge_unique_ptr = g2o::make_unique<g2o::EdgeXYPrior>();
  prior_edge_unique_ptr->setId(0);
  prior_edge_unique_ptr->setVertex(0, optimizer_pose_graph_.vertex(kSensorPoseVertexId));
  prior_edge_unique_ptr->setMeasurement(sensor_trans_motion_estimate);
  prior_edge_unique_ptr->setInformation(1e-20 * Eigen::Matrix<double, 2, 2>::Identity());
  optimizer_pose_graph_.addEdge(std::move(prior_edge_unique_ptr).get());
  prior_edge_unique_ptr.release();
}

void TransVelEstimator::CalculateCovarianceMatrix(
    const Eigen::Matrix<double, Eigen::Dynamic, 2>& A_inliers,
    const Eigen::Matrix<double, Eigen::Dynamic, 1>& range_inliers,
    Eigen::Matrix<double, 3, 3>& sensor_motion_covariance_matrix) {

  // Extracts the 2x2 information matrix of the translational velocity estimate
  // from the g2o pose graph
  Eigen::Matrix<double, 2, 2> information_matrix;
  information_matrix <<
    static_cast<g2o::VertexPointXY*>(
      optimizer_pose_graph_.vertex(kSensorPoseVertexId))->hessian(0, 0),
    static_cast<g2o::VertexPointXY*>(
      optimizer_pose_graph_.vertex(kSensorPoseVertexId))->hessian(0, 1),
    static_cast<g2o::VertexPointXY*>(
      optimizer_pose_graph_.vertex(kSensorPoseVertexId))->hessian(1, 0),
    static_cast<g2o::VertexPointXY*>(
      optimizer_pose_graph_.vertex(kSensorPoseVertexId))->hessian(1, 1);

  // Inverts the trans. vel. information matrix to create the upper-left 2x2
  // submatrix in the sensor motion covariance matrix (corresponds to v_x, v_y)
  sensor_motion_covariance_matrix.topLeftCorner<2, 2>() = information_matrix.inverse();

  // RADARODO calculates yaw-rate (omega_z) based on v_x and v_y, so there are covariance
  // terms between the two. However, the yaw rate estimate (and its expected standard
  // deviation has not yet been calculated). Thus, those covariance terms are calculated
  // based on the partial derivative of yaw rate with respect to v_x and v_y, but are
  // left unscaled with respect to the yaw rate standard deviation. These terms will be
  // scaled accordingly later in the Rotational Velocity Estimator once this standard
  // deviation is calculated.

  // Calculate partial derivatives
  const double partial_omegaz_partial_thetadot = -1;
  const Eigen::Matrix<double, Eigen::Dynamic, 1> partial_omegaz_partial_vx =
    - A_inliers.rightCols<1>().cwiseQuotient(range_inliers);
  const Eigen::Matrix<double, Eigen::Dynamic, 1> partial_omegaz_partial_vy =
    A_inliers.leftCols<1>().cwiseQuotient(range_inliers);

  // Use partial derivatives to calculate correlation coefficients, but only scale
  // with regards to v_x right now
  sensor_motion_covariance_matrix(2, 0) =
    std::sqrt(sensor_motion_covariance_matrix(0, 0)) *
    (partial_omegaz_partial_thetadot * partial_omegaz_partial_vx).mean();

  // Use partial derivatives to calculate correlation coefficients, but only scale
  // with regards to v_y right now
  sensor_motion_covariance_matrix(2, 1) =
    std::sqrt(sensor_motion_covariance_matrix(1, 1)) *
    (partial_omegaz_partial_thetadot * partial_omegaz_partial_vy).mean();

  sensor_motion_covariance_matrix(0, 2) = sensor_motion_covariance_matrix(2, 0);
  sensor_motion_covariance_matrix(1, 2) = sensor_motion_covariance_matrix(2, 1);
}
