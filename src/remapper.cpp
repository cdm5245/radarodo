/********************************COPYRIGHT CHRIS D. MONACO, 2019**********************************/
#include <cmath>
#include <functional>
#include <limits>
#include <thread>

#include <opencv2/imgproc/imgproc.hpp>

#include <radarodo/remapper.h>

typedef Remapper::Polar Polar;
typedef Remapper::Cartesian Cartesian;
typedef Remapper::Exact Exact;
typedef Remapper::Approx Approx;

Remapper::Remapper(const double long_range_max,
                   const double long_range_plus_minus_fov_rad,
                   const double mid_range_max,
                   const double mid_range_plus_minus_fov_rad,
                   const uint polar_image_width,
                   const uint cartesian_image_width,
                   const uint image_height,
                   const double meters_per_pixel,
                   const double radians_per_pixel,
                   const double polar_image_u0,
                   const double cartesian_image_u0,
                   const double image_v0)
  : long_range_max_(long_range_max),
    long_range_plus_minus_fov_rad_(long_range_plus_minus_fov_rad),
    mid_range_max_(mid_range_max),
    mid_range_plus_minus_fov_rad_(mid_range_plus_minus_fov_rad),
    polar_image_width_(polar_image_width),
    cartesian_image_width_(cartesian_image_width),
    image_height_(image_height),
    meters_per_pixel_(meters_per_pixel),
    radians_per_pixel_(radians_per_pixel),
    polar_image_u0_(polar_image_u0),
    cartesian_image_u0_(cartesian_image_u0),
    image_v0_(image_v0) {
  // Generates the polar 2 cartesian and cartesian 2 polar pixel maps in paralell
  // using two separate threads
  std::thread thread_1(&Remapper::GenerateMap<Polar, Cartesian>, this);
  std::thread thread_2(&Remapper::GenerateMap<Cartesian, Polar>, this);

  thread_1.join();
  thread_2.join();
}

template <>
void Remapper::GenerateMap<Polar, Cartesian>() {
  // Calculates images containing cartesian image u- and v- pixel coordinates
  cv::Mat cartesian_image_u_coordinates, cartesian_image_v_coordinates;
  GeneratePixelCoordinateImages(cartesian_image_width_,
                                image_height_,
                                cartesian_image_u_coordinates,
                                cartesian_image_v_coordinates);

  // Calculates x- and y- Cartesian coordinates [m] for each Cartesian image pixel
  const cv::Mat x_values = (- cartesian_image_v_coordinates + image_v0_) * meters_per_pixel_;
  const cv::Mat y_values =
    (- cartesian_image_u_coordinates + cartesian_image_u0_) * meters_per_pixel_;

  // Transforms cartesian coordinates [m, m] to polar coordinates [m, rad]
  cv::Mat r_values, theta_rad_values;
  cv::cartToPolar(x_values,
                 y_values,
                 r_values,
                 theta_rad_values);

  // Ensure azimuthal angles are within the proper range to describe azimuthal deviations
  // from the longitudinal axis
  cv::subtract(theta_rad_values, 2.0 * M_PI, theta_rad_values, theta_rad_values > M_PI);

  // Calculates the pixel lookup maps for both u- and v- coordinates to transform polar images to
  // cartesian images
  polar2cart_u_map_ = - theta_rad_values / radians_per_pixel_ + polar_image_u0_;
  polar2cart_v_map_ = - r_values / meters_per_pixel_ + image_v0_;
}

template <>
void Remapper::GenerateMap<Cartesian, Polar>() {
  // Calculates images containing polar image u- and v- pixel coordinates
  cv::Mat polar_image_u_coordinates, polar_image_v_coordinates;
  GeneratePixelCoordinateImages(polar_image_width_,
                                image_height_,
                                polar_image_u_coordinates,
                                polar_image_v_coordinates);

  // Calculates range and theta polar coordinates [m, rad] for each polar image pixel
  const cv::Mat r_values = (- polar_image_v_coordinates + image_v0_) * meters_per_pixel_;
  const cv::Mat theta_rad_values =
    (- polar_image_u_coordinates + polar_image_u0_) * radians_per_pixel_;

  // Transforms polar coordinates [m, rad] to cartesian coordinates [m, m]
  cv::Mat x_values, y_values;
  cv::polarToCart(r_values,
                  theta_rad_values,
                  x_values,
                  y_values);

  // Calculates the pixel lookup maps for both u- and v- coordinates to transform cartesian images
  // to polar images
  cart2polar_u_map_ = - y_values / meters_per_pixel_ + cartesian_image_u0_;
  cart2polar_v_map_ = - x_values / meters_per_pixel_ + image_v0_;
}

void Remapper::GenerateBlurredPolarImage(
    const Eigen::Matrix<double, 3, 3>& sensor_motion_covariance_matrix,
    const double delta_t,
    cv::Mat& polar_image,
    cv::Mat& blurred_polar_image) {
  // Calculate the pixel equivalents of the longitudinal and lateral translational velocity
  // estimate's expected standard deviation
  const double sigma_tx_pixels =
    std::sqrt(sensor_motion_covariance_matrix(0, 0)) * delta_t / meters_per_pixel_;
  const double sigma_ty_pixels =
    std::sqrt(sensor_motion_covariance_matrix(1, 1)) * delta_t / meters_per_pixel_;

  // Set Gaussian blur kernel size to +/- sigma
  uint kernel_width = std::round(2 * sigma_ty_pixels);
  uint kernel_height = std::round(2 * sigma_tx_pixels);

  // If the Gaussian kernel size is 1x1 or smaller, blurring is inconsequential, so set
  // blurred polar image to the original polar image
  if (kernel_width < 2 && kernel_height < 2) {
    blurred_polar_image = polar_image;
    return;
  }

  // Ensure kernel size is odd
  if (kernel_width % 2 == 0) ++kernel_width;
  if (kernel_height % 2 == 0) ++kernel_height;

  // Remap polar image into its cartesian counterpart
  cv::Mat cartesian_image;
  Remap<Polar, Cartesian>(polar_image, cartesian_image);

  // Blur with gaussian kernel calculated from trans. vel. estimate uncertainty (defined in
  // Cartesian coordinate system).
  cv::GaussianBlur(cartesian_image,
                   cartesian_image,
                   cv::Size(kernel_width, kernel_height),
                   sigma_ty_pixels,
                   sigma_tx_pixels,
                   cv::BORDER_REPLICATE);

  // After blurring, transform back into a polar image
  Remap<Cartesian, Polar>(cartesian_image, blurred_polar_image);
}

template <>
void Remapper::CalculatePolarPixelTranslations<Exact>(
    const Eigen::Vector3d& sensor_motion_estimate,
    const double delta_t,
    const Eigen::Array<double, Eigen::Dynamic, 2>& previous_polar_pixel_points,
    Eigen::Array<double, Eigen::Dynamic, 2>& polar_pixel_translations) {
  // If translation is not necessary, set translations to zero and return
  if (!previous_polar_pixel_points.rows() ||
      (!sensor_motion_estimate.x() && !sensor_motion_estimate.y())) {
    polar_pixel_translations =
      Eigen::Array<double, Eigen::Dynamic, 2>::Zero(previous_polar_pixel_points.rows(), 2);
    return;
  }

  // For all subsequent calculations, each row corresponds to an individual pixel point.
  // Each column corresponds to a different variable.

  // Convert polar image pixel coordinates (u, v) to polar coordinates (range, theta)
  Eigen::Array<double, Eigen::Dynamic, 2> point_polar_coordinates =
    Eigen::Array<double, Eigen::Dynamic, 2>(previous_polar_pixel_points.rows(), 2);
  point_polar_coordinates.leftCols<1>() =
    (- previous_polar_pixel_points.rightCols<1>() + image_v0_) *
    meters_per_pixel_;
  point_polar_coordinates.rightCols<1>() =
    (- previous_polar_pixel_points.leftCols<1>() + polar_image_u0_) *
    radians_per_pixel_;

  // Convert polar coordinates (range, theta) to cartesian coordinates (x, y)
  Eigen::Array<double, Eigen::Dynamic, 2> point_cartesian_coordinates =
    Eigen::Array<double, Eigen::Dynamic, 2>(previous_polar_pixel_points.rows(), 2);
  point_cartesian_coordinates.leftCols<1>() =
    point_polar_coordinates.leftCols<1>() * point_polar_coordinates.rightCols<1>().cos();
  point_cartesian_coordinates.rightCols<1>() =
    point_polar_coordinates.leftCols<1>() * point_polar_coordinates.rightCols<1>().sin();

  // Translate point's cartesian coordinates according to the sensor's translational motion
  // in the cartesian coordinate system
  point_cartesian_coordinates.leftCols<1>() -= sensor_motion_estimate.x() * delta_t;
  point_cartesian_coordinates.rightCols<1>() -= sensor_motion_estimate.y() * delta_t;

  // Transform the translated points cartesian coordinate (x, y) back to transformed polar
  // coordinates (r, theta)
  Eigen::Array<double, Eigen::Dynamic, 2> transformed_point_polar_coordinates =
      Eigen::Array<double, Eigen::Dynamic, 2>(previous_polar_pixel_points.rows(), 2);
  transformed_point_polar_coordinates.leftCols<1>() =
    point_cartesian_coordinates.matrix().rowwise().norm();
  transformed_point_polar_coordinates.rightCols<1>() =
    point_cartesian_coordinates.rightCols<1>().binaryExpr(
      point_cartesian_coordinates.leftCols<1>(), [] (double y, double x) {
        return std::atan2(y, x);
      });

  // Calculate the change of the points polar coordinates from the transformation
  const Eigen::Array<double, Eigen::Dynamic, 2> delta_point_polar_coordinates =
    transformed_point_polar_coordinates - point_polar_coordinates;

  // Convert the polar coordinate change (delta r, delta theta) to their polar RADAR image
  // pixel equivalent (delta u, delta v) to yield the pixel translations
  polar_pixel_translations =
    Eigen::Array<double, Eigen::Dynamic, 2>(previous_polar_pixel_points.rows(), 2);
  polar_pixel_translations <<
    - delta_point_polar_coordinates.rightCols<1>() / radians_per_pixel_,
    - delta_point_polar_coordinates.leftCols<1>() / meters_per_pixel_;
}

template <>
void Remapper::CalculatePolarPixelTranslations<Approx>(
    const Eigen::Vector3d& sensor_motion_estimate,
    const double delta_t,
    const Eigen::Array<double, Eigen::Dynamic, 2>& previous_polar_pixel_points,
    Eigen::Array<double, Eigen::Dynamic, 2>& polar_pixel_translations) {
  // If translation is not necessary, set translations to zero and return
  if (!previous_polar_pixel_points.rows() ||
      (!sensor_motion_estimate.x() && !sensor_motion_estimate.y())) {
    polar_pixel_translations =
      Eigen::Array<double, Eigen::Dynamic, 2>::Zero(previous_polar_pixel_points.rows(), 2);
    return;
  }

  // For all subsequent calculations, each row corresponds to an individual pixel point.
  // Each column corresponds to a different variable.

  // Convert polar image pixel coordinates (u, v) to polar coordinates (range, theta)
  Eigen::Array<double, Eigen::Dynamic, 2> point_polar_coordinates =
    Eigen::Array<double, Eigen::Dynamic, 2>(previous_polar_pixel_points.rows(), 2);
  point_polar_coordinates.leftCols<1>() =
    (- previous_polar_pixel_points.rightCols<1>() + image_v0_) *
    meters_per_pixel_;
  point_polar_coordinates.rightCols<1>() =
    (- previous_polar_pixel_points.leftCols<1>() + polar_image_u0_) *
    radians_per_pixel_;

  // Calculate the partial time derivatives of (range, theta) as a function of the
  // sensor's translational velocities
  Eigen::Array<double, Eigen::Dynamic, 2> polar_time_derivatives =
    Eigen::Array<double, Eigen::Dynamic, 2>(previous_polar_pixel_points.rows(), 2);
  polar_time_derivatives.leftCols<1>() =
    - point_polar_coordinates.rightCols<1>().cos() * sensor_motion_estimate.x()
    - point_polar_coordinates.rightCols<1>().sin() * sensor_motion_estimate.y();
  polar_time_derivatives.rightCols<1>() =
    (point_polar_coordinates.rightCols<1>().sin() * sensor_motion_estimate.x()
     - point_polar_coordinates.rightCols<1>().cos() * sensor_motion_estimate.y()) /
    point_polar_coordinates.leftCols<1>();

  // Use the time between measurements, delta_t, to convert these derivatives into polar coordinate
  // changes, and then convert them to their pixel equivalents (u, v) to yield the pixel
  // translation
  polar_pixel_translations =
    Eigen::Array<double, Eigen::Dynamic, 2>(previous_polar_pixel_points.rows(), 2);
  polar_pixel_translations <<
    - polar_time_derivatives.rightCols<1>() * delta_t / radians_per_pixel_,
    - polar_time_derivatives.leftCols<1>() * delta_t / meters_per_pixel_;
}

const cv::Mat& Remapper::polar_non_field_of_view_mask() {
  // Generates the mask if it has not yet been generated
  if (polar_non_field_of_view_mask_.empty()) GenerateMask<Polar>();

  return polar_non_field_of_view_mask_;
}

const cv::Mat& Remapper::cartesian_non_field_of_view_mask() {
  // Generates the mask if it has not yet been generated
  if (cartesian_non_field_of_view_mask_.empty()) GenerateMask<Cartesian>();

  return cartesian_non_field_of_view_mask_;
}

template<>
void Remapper::Remap<Polar, Cartesian>(const cv::Mat& from, cv::Mat& to) {
  cv::remap(from,
            to,
            polar2cart_u_map_,
            polar2cart_v_map_,
            CV_INTER_LINEAR);
}

template<>
void Remapper::Remap<Cartesian, Polar>(const cv::Mat& from, cv::Mat& to) {
  cv::remap(from,
            to,
            cart2polar_u_map_,
            cart2polar_v_map_,
            CV_INTER_LINEAR);
}

void Remapper::GeneratePixelCoordinateImages(const uint image_width,
                                             const uint image_height,
                                             cv::Mat& u_values_image,
                                             cv::Mat& v_values_image) {
  // Creates a vector of u values up to, but not including the image width
  std::vector<ushort> u_values_vector(image_width);
  std::iota(u_values_vector.begin(), u_values_vector.end(), 0.0);

  // Creates a vector of v values up to, but not including the image height
  std::vector<ushort> v_values_vector(image_height);
  std::iota(v_values_vector.begin(), v_values_vector.end(), 0.0);

  // Turns these vectors into equivalent row or column image matrices
  cv::Mat cv_u_values_vector(1,
                             image_width,
                             CV_16UC1,
                             u_values_vector.data());
  cv::Mat cv_v_values_vector(image_height,
                             1,
                             CV_16UC1,
                             v_values_vector.data());

  // Repeats these rows or columns in the appropriate direction to create the desired image size in
  // which each pixel describes its own u- or v- pixel coordinate
  cv::repeat(cv_u_values_vector,
            image_height,
            1,
            u_values_image);

  cv::repeat(cv_v_values_vector,
            1,
            image_width,
            v_values_image);

  // Converts these ushort images to floating point images for later mathematical operations
  u_values_image.convertTo(u_values_image, CV_32FC1);
  v_values_image.convertTo(v_values_image, CV_32FC1);
}



template<>
void Remapper::GenerateMask<Polar>() {
  polar_non_field_of_view_mask_ =
    cv::Mat::zeros(image_height_, polar_image_width_, CV_8UC1);

  // Create rectangle describing the left (+theta) portion of the RADAR polar image that is beyond
  // both the mid-range and long-range field of view
  const cv::Rect long_left_non_field_of_view(
    0,
    0,
    std::round(- long_range_plus_minus_fov_rad_ / radians_per_pixel_ + polar_image_u0_),
    std::round((long_range_max_ - mid_range_max_) / meters_per_pixel_));

  // Create rectangle describing the right (-theta) portion of the RADAR polar image that is beyond
  // both the mid-range and long-range field of view
  cv::Rect long_right_non_field_of_view(long_left_non_field_of_view);
  long_right_non_field_of_view.x =
    std::round(+ long_range_plus_minus_fov_rad_ / radians_per_pixel_ + polar_image_u0_) + 1;

  // Set all pixels within the non-field-of-view rectangles to 255 to flag that
  // these pixels are not part of the RADAR's field of view
  polar_non_field_of_view_mask_(long_left_non_field_of_view) =
    std::numeric_limits<uchar>::max();
  polar_non_field_of_view_mask_(long_right_non_field_of_view) =
    std::numeric_limits<uchar>::max();
}

template<>
void Remapper::GenerateMask<Cartesian>() {
  cartesian_non_field_of_view_mask_ =
    cv::Mat::zeros(image_height_, cartesian_image_width_, CV_8UC1);

  // Calculates the square of the long- and mid-range max ranges
  const double long_range_max_2 = long_range_max_ * long_range_max_;
  const double mid_range_max_2 = mid_range_max_ * mid_range_max_;

  // For each pixel in the cartesian image mask:
  cartesian_non_field_of_view_mask_.forEach<uchar>(
    [&] (uchar& pixel_value, const int position[]) -> void {
      // Calculate the pixel's corresponding x, y, range squared, and absolute value of its
      // azimuthal angle in radians
      const double x = (- position[0] + image_v0_) * meters_per_pixel_;
      const double y = (- position[1] + cartesian_image_u0_) * meters_per_pixel_;
      const double r2 = x * x + y * y;
      const double abs_theta_radians = std::abs(std::atan(y / x));

      // If it is beyond both the mid- and long-range field of views, mark the pixel with 255 to
      // flag that it is not part of the RADAR's field of view
      if ((r2 > long_range_max_2 ||
            abs_theta_radians > long_range_plus_minus_fov_rad_) &&
          (r2 > mid_range_max_2 ||
            abs_theta_radians > mid_range_plus_minus_fov_rad_)) {
          pixel_value = std::numeric_limits<uchar>::max();
      }
    });
}
