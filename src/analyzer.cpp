/********************************COPYRIGHT CHRIS D. MONACO, 2019**********************************/
#include <iomanip>

#include <radarodo/analyzer.h>

Analyzer::Analyzer(const std::string& package_path,
                   const bool record_results,
                   const bool record_runtimes) {
  if (record_results) {
    gps_.motion_subscriber_ = nh_.subscribe(
      "/as_rx/vehicle_motion",
      20,
      &Analyzer::GpsMotionCallback,
      this);

    radarodo_.motion_subscriber_ = nh_.subscribe(
      "/radar_egomotion",
      20,
      &Analyzer::RadarodoMotionCallback,
      this);

    results_csv_.open(package_path + "/result_logs/results.csv");
    results_csv_ << "gps_time" << "," <<
                    "gps_vx" << "," <<
                    "gps_vy" << "," <<
                    "gps_yaw_rate" << "," <<
                    "radarodo_seq" << "," <<
                    "radarodo_time" << "," <<
                    "radarodo_vx" << "," <<
                    "radarodo_vy" << "," <<
                    "radarodo_yaw_rate" << "," <<
                    "radarodo_vx_cov" << "," <<
                    "radarodo_vy_cov" << "," <<
                    "radarodo_vxvy_cov" << "," <<
                    "radarodo_yaw_rate_cov" << std::endl;
    results_csv_ << std::setprecision(20);
  }

  if (record_runtimes) {
    runtimes_csv_.open(package_path + "/result_logs/runtimes.csv");
    runtimes_csv_ << "trans_vel_estimation_us" << "," <<
                      "radar_image_generation_us" << "," <<
                      "remapping_us" << "," <<
                      "rot_vel_estimation_us" << "," <<
                      "point_extraction_preparation_us" << std::endl;
  }
}

void Analyzer::GpsMotionCallback(
    const geometry_msgs::TwistStampedConstPtr gps_motion_msg_ptr) {
  gps_.time = gps_motion_msg_ptr->header.stamp.toSec();
  gps_.vx = gps_motion_msg_ptr->twist.linear.x;
  gps_.vy = 0;
  gps_.yaw_rate = gps_motion_msg_ptr->twist.angular.z;
}

void Analyzer::RadarodoMotionCallback(
    const geometry_msgs::TwistWithCovarianceStampedConstPtr radarodo_motion_msg_ptr) {
  radarodo_.seq = radarodo_motion_msg_ptr->header.seq;
  radarodo_.time = radarodo_motion_msg_ptr->header.stamp.toSec();
  radarodo_.vx = radarodo_motion_msg_ptr->twist.twist.linear.x;
  radarodo_.vy = radarodo_motion_msg_ptr->twist.twist.linear.y;
  radarodo_.yaw_rate = radarodo_motion_msg_ptr->twist.twist.angular.z;
  radarodo_.vx_cov = radarodo_motion_msg_ptr->twist.covariance[0];
  radarodo_.vy_cov = radarodo_motion_msg_ptr->twist.covariance[7];
  radarodo_.vxvy_cov = radarodo_motion_msg_ptr->twist.covariance[1];
  radarodo_.yaw_rate_cov = radarodo_motion_msg_ptr->twist.covariance[35];

  RecordResults();
}

void Analyzer::RecordResults() {
  results_csv_ << gps_.time << "," <<
                  gps_.vx << "," <<
                  gps_.vy << "," <<
                  gps_.yaw_rate  << "," <<
                  radarodo_.seq << "," <<
                  radarodo_.time << "," <<
                  radarodo_.vx << "," <<
                  radarodo_.vy << "," <<
                  radarodo_.yaw_rate << "," <<
                  radarodo_.vx_cov << "," <<
                  radarodo_.vy_cov << "," <<
                  radarodo_.vxvy_cov << "," <<
                  radarodo_.yaw_rate_cov << std::endl;
}

void Analyzer::RecordRuntimes(const int trans_vel_estimation_us,
                              const int radar_image_generation_us,
                              const int remapping_us,
                              const int rot_vel_estimation_us,
                              const int point_extraction_preparation_us) {
  runtimes_csv_ << trans_vel_estimation_us << "," <<
                   radar_image_generation_us << "," <<
                   remapping_us << "," <<
                   rot_vel_estimation_us << "," <<
                   point_extraction_preparation_us << std::endl;
}
