/********************************COPYRIGHT CHRIS D. MONACO, 2019**********************************/
#include <iostream>
#include <cmath>
#include <numeric>
#include <iomanip>
#include <algorithm>
#include <fstream>

#include <ros/package.h>
#include <geometry_msgs/TwistWithCovarianceStamped.h>

#include <radarodo/egomotion_estimator.h>

///////////// IMPLEMENTATION-SPECIFIC PARAMETERS THAT CAN BE MANUALLY SET BY END-USER /////////////
static constexpr double kLongRangeMax = 174.0;          // RADAR long-range scan max range [m]
static constexpr double kLongRangePlusMinusFOVDegrees = 10.0;
                                                        // RADAR long-range scan +/- field-of-view
                                                        //   [deg]
static constexpr double kMidRangeMax = 60.0;            // RADAR mid-range scan max range [m]
static constexpr double kMidRangePlusMinusFOVDegrees = 45.0;
                                                        // RADAR mid-range scan +/- field-of-view
                                                        //   [deg]
static constexpr uint kImageHeight = 1000;              // RADAR image height [px.]
static constexpr double kMaxReturndB = 40.0;            // RADAR extracted point target max dB
static constexpr double kMinReturndB = -10.0;           // RADAR extracted point target min dB
static constexpr uint kTransVelRANSACIterations = 15;   // Iterations for trans. vel. estimation
                                                        //   RANSAC inlier extraction
static constexpr uint kMaxNumTransVelOptimizationIterations = 10;
                                                        // Max number of iterations for trans. vel.
                                                        //   estimation non-linear optimization
static constexpr bool kVerboseTransVelOptimization = false;
                                                        // Flag to make trans. vel. optimization
                                                        //   verbose while optimizing
static constexpr double kExtractedPointsWeightFracThreshold = 0.1;
                                                        // Fraction of image max pixel intensity
                                                        //   that determines min. intensity
                                                        //   threshold for a point to be
                                                        //   extracted
static constexpr double kExtractedPointsImageFraction = 3.0e-3;
                                                        // Maximum fraction of RADAR image pixels
                                                        //   that can be extracted points
static constexpr uint kMaxNumRotVelOptimizationIterations = 10;
                                                        // Maximum number of iterations for rot.
                                                        //   vel. estimation optimization
static constexpr bool kVerboseRotVelOptimization = false;
                                                        // Flag to make rot. vel. optimization
                                                        //   verbose while optimizing
static constexpr double kRangeRateSigmaMetersPerSec = 0.12;
                                                        // RADAR extracted target range rate
                                                        //   measurement standard deviation [m/s]
static constexpr double kRADARLongRangeThetaSigmaDeg = 0.5;
                                                        // RADAR extracted target azimuthal angle
                                                        //  standard deviation [deg] for long-range
                                                        //  scan
static constexpr double kRADARMidRangeThetaSigmaDeg = 1.0;
                                                        // RADAR extracted target azimuthal angle
                                                        //  standard deviation [deg] for mid-range
                                                        //  scan
static constexpr double kRADARRangeSigmaMeters = 0.375;
                                                        // RADAR extracted target range measurement
                                                        // standard deviation [m]
static constexpr double kRADARAzimuthalOffsetDeg = 2.8;
                                                        // Set azimuthal mounting offset of RADAR
                                                        //   from vehicle's longitudinal x-axis

///////////// CONSTANT PARAMETERS CALCULATED FROM MANUALLY SET PARAMETERS /////////////
static constexpr double kLongRangePlusMinusFOVRadians = kLongRangePlusMinusFOVDegrees *
                                                          M_PI / 180.0;
static constexpr double kMidRangePlusMinusFOVRadians = kMidRangePlusMinusFOVDegrees *
                                                         M_PI / 180.0;
static constexpr double kMetersPerPixel = kLongRangeMax / kImageHeight;
// Determine Cartesian image width by determining if long-range or mid-range scan extends further
// along the y- lateral direction
static constexpr uint kCartesianImageWidth =
  std::round(2.0 * kLongRangeMax * std::sin(kLongRangePlusMinusFOVRadians)/ kMetersPerPixel) >
  std::round(2.0 * kMidRangeMax * std::sin(kMidRangePlusMinusFOVRadians)/ kMetersPerPixel) ?
  std::round(2.0 * kLongRangeMax * std::sin(kLongRangePlusMinusFOVRadians)/ kMetersPerPixel) :
  std::round(2.0 * kMidRangeMax * std::sin(kMidRangePlusMinusFOVRadians)/ kMetersPerPixel);
// RADAR Cartesian and Polar images share a set image height. They also share a calculated image,
// but the code is flexible enough to handle different widths.
static constexpr uint kPolarImageWidth = kCartesianImageWidth;
// Polar image's degrees per pixels are determined by which scan has a larger field of view, given
// the set image width
static constexpr double kDegreesPerPixel =
  kLongRangePlusMinusFOVDegrees > kMidRangePlusMinusFOVDegrees ?
  2.0 * kLongRangePlusMinusFOVDegrees / (kPolarImageWidth - 1) :
  2.0 * kMidRangePlusMinusFOVDegrees / (kPolarImageWidth - 1);
static constexpr double kRadiansPerPixel = kDegreesPerPixel * M_PI / 180.0;
// "Center" of RADAR images are at the bottom center. Cartesian image has x-axis pointing upward,
// y-axis to the left (robotics coordinate system from birds-eye view). Polar image has r-axis
// going upward, theta-axis to the left
static constexpr double kImageV0 = kImageHeight;
static constexpr double kCartesianImageU0 = (kCartesianImageWidth - 1) / 2.0;
static constexpr double kPolarImageU0 = (kPolarImageWidth - 1) / 2.0;
static constexpr double kRangeRateRANSACInlierThreshold = 2.0 * kRangeRateSigmaMetersPerSec;
static constexpr uint kMaxNumExtractedPoints =
  std::round(kExtractedPointsImageFraction * kImageHeight * kPolarImageWidth);
static constexpr double kRADARLongRangeThetaSigmaRad = kRADARLongRangeThetaSigmaDeg * M_PI / 180.0;
static constexpr double kRADARMidRangeThetaSigmaRad = kRADARMidRangeThetaSigmaDeg * M_PI / 180.0;
static constexpr double kRADARMedianThetaSigmaRad =
  (kRADARLongRangeThetaSigmaRad + kRADARMidRangeThetaSigmaRad) / 2.0;
static constexpr double kRADARAzimuthalOffsetRad = kRADARAzimuthalOffsetDeg * M_PI / 180.0;

EgomotionEstimator::EgomotionEstimator()
  : ros_node_path_(ros::package::getPath("radarodo")),
    mid_range_scan_stored_(false),
    long_range_scan_stored_(false),
    radar_image_generator_(kPolarImageWidth,
                           kImageHeight,
                           kMetersPerPixel,
                           kRadiansPerPixel,
                           kPolarImageU0,
                           kImageV0,
                           kRADARRangeSigmaMeters,
                           kRADARMedianThetaSigmaRad),
    trans_vel_estimator_(kTransVelRANSACIterations,
                         kRangeRateRANSACInlierThreshold,
                         kRangeRateSigmaMetersPerSec,
                         kRADARMedianThetaSigmaRad,
                         kMaxNumTransVelOptimizationIterations,
                         kVerboseTransVelOptimization),
    remapper_(kLongRangeMax,
              kLongRangePlusMinusFOVRadians,
              kMidRangeMax,
              kMidRangePlusMinusFOVRadians,
              kPolarImageWidth,
              kCartesianImageWidth,
              kImageHeight,
              kMetersPerPixel,
              kRadiansPerPixel,
              kPolarImageU0,
              kCartesianImageU0,
              kImageV0),
    rot_vel_estimator_(kRadiansPerPixel,
                       kExtractedPointsWeightFracThreshold,
                       kMaxNumExtractedPoints,
                       kMaxNumRotVelOptimizationIterations,
                       kVerboseRotVelOptimization),
    analyzer_(ros_node_path_, true, false) {
  radar_data_subscriber_ = nh_.subscribe(
    "/parsed_tx/esr_eth_tx_msg",
    20,
    &EgomotionEstimator::RadarDataCallback,
    this);

  egomotion_publisher_ =
    nh_.advertise<geometry_msgs::TwistWithCovarianceStamped>("radar_egomotion", 20);

  // Reserve 128 slots for RADAR detections since both mid- and long-range scans can
  // feasibly report 64 targets
  valid_radar_detections_.reserve(128);
}

void EgomotionEstimator::RadarDataCallback(
    const delphi_esr_msgs::EsrEthTxConstPtr radar_data_msg_ptr) {
  // Only continue if both RADAR scans are stored and there are at least three detected targets
  if (!StoreRadarData(*radar_data_msg_ptr) || valid_radar_detections_.size() < 3) return;

  // Estimate translational velocity of RADAR sensor. Return if estimate is invalid.
  Eigen::Vector3d sensor_motion_estimate;
  Eigen::Matrix<double, 3, 3> sensor_motion_covariance_matrix;
  if (!trans_vel_estimator_.Estimate(valid_radar_detections_,
                                     sensor_motion_estimate,
                                     sensor_motion_covariance_matrix)) return;

  // In lieu of real RADAR image, generate emulated polar image from indivdual reported detections
  cv::Mat polar_image;
  radar_image_generator_.GeneratePolarImage(valid_radar_detections_, polar_image);

  // If no previous points are stored (i.e. this is the first iteration), prepare for the next
  // measurement and return
  if (!rot_vel_estimator_.previous_points_stored()) {
    PrepareForNextMeasurement(polar_image, radar_data_msg_ptr->header.stamp);
    return;
  }

  // Calculate time between RADAR image measurements. If zero, return.
  const double delta_t = (radar_data_msg_ptr->header.stamp - previous_msg_timestamp_).toSec();
  if (!delta_t) return;

  // Blur polar image according to translational velocity estimate
  cv::Mat blurred_polar_image;
  remapper_.GenerateBlurredPolarImage(sensor_motion_covariance_matrix,
                                      delta_t,
                                      polar_image,
                                      blurred_polar_image);

  // Calculate necessary translation of extracted pixel points from previous RADAR image
  // according to translational velocity estimate. Then, translate these stored points
  // within the Rotational Velocity Estimator.
  Eigen::Array<double, Eigen::Dynamic, 2> polar_pixel_translations;
  remapper_.CalculatePolarPixelTranslations<Remapper::Approx>(
    sensor_motion_estimate,
    delta_t,
    rot_vel_estimator_.extracted_point_data().leftCols<2>(),
    polar_pixel_translations);
  rot_vel_estimator_.TranslatePreviousPoints(polar_pixel_translations);

  // Estimate RADAR yaw-rate with rotational velocity estimator
  sensor_motion_estimate.z() = rot_vel_estimator_.Estimate(blurred_polar_image,
                                                           delta_t,
                                                           sensor_motion_covariance_matrix);

  // Publish RADAR motion estimate and covariance matrix via ROS
  PublishSensorMotionMsg(radar_data_msg_ptr->header,
                         sensor_motion_estimate,
                         sensor_motion_covariance_matrix);

  // Prepare data for next measurement (involves extracted pixel points from RADAR image)
  PrepareForNextMeasurement(polar_image, radar_data_msg_ptr->header.stamp);
}

void EgomotionEstimator::PrepareForNextMeasurement(
    const cv::Mat& polar_image,
    const ros::Time& previous_msg_timestamp) {
  // Extract pixel points from current (unblurred) RADAR polar image
  rot_vel_estimator_.StoreMeasurement(polar_image);

  // Clear stored data and store current timestamp for next measurement
  mid_range_scan_stored_ = false;
  long_range_scan_stored_ = false;
  valid_radar_detections_.clear();
  previous_msg_timestamp_ = previous_msg_timestamp;
}

bool EgomotionEstimator::StoreRadarData(const delphi_esr_msgs::EsrEthTx& radar_data_msg) {
  // Mark if this scan's data corresponds to the mid-range or long-range scan
  switch (radar_data_msg.xcp_scan_type) {
    case 1:
      mid_range_scan_stored_ = true;
      break;
    case 2:
      long_range_scan_stored_ = true;
      break;
  }

  // Iterate through all reported targets for the scan
  for (uint target_idx = 0;
      target_idx < radar_data_msg.target_report_range.size();
      ++target_idx) {
    // Only valid targets have a non-zero range
    if (!radar_data_msg.target_report_range[target_idx]) continue;

    // Stores the "weight", range, azimuthal angle [rad] (converted to rad and compensated for the
    // RADAR's azimuthal mounting offset), range rate, and scan type of valid detected target
    valid_radar_detections_.emplace_back(
      (radar_data_msg.target_report_amplitude[target_idx] - kMinReturndB) /
        (kMaxReturndB - kMinReturndB),
      radar_data_msg.target_report_range[target_idx],
      (radar_data_msg.target_report_theta[target_idx] + kRADARAzimuthalOffsetDeg) * M_PI / 180.0,
      radar_data_msg.target_report_range_rate[target_idx],
      radar_data_msg.xcp_scan_type);
  }

  // Return true only if data from both RADAR scans are stored
  return (mid_range_scan_stored_ && long_range_scan_stored_) ? true : false;
}

void EgomotionEstimator::PublishSensorMotionMsg(
    const std_msgs::Header& radar_data_msg_header,
    const Eigen::Vector3d& sensor_motion_estimate,
    const Eigen::Matrix<double, 3, 3>& sensor_motion_covariance_matrix) {
  geometry_msgs::TwistWithCovarianceStamped sensor_motion_msg;
  sensor_motion_msg.header = radar_data_msg_header;

  sensor_motion_msg.twist.twist.linear.x = sensor_motion_estimate.x();
  sensor_motion_msg.twist.twist.linear.y = sensor_motion_estimate.y();
  sensor_motion_msg.twist.twist.angular.z = sensor_motion_estimate.z();

  sensor_motion_msg.twist.covariance[0] = sensor_motion_covariance_matrix(0, 0);
  sensor_motion_msg.twist.covariance[1] = sensor_motion_covariance_matrix(0, 1);
  sensor_motion_msg.twist.covariance[5] = sensor_motion_covariance_matrix(0, 2);
  sensor_motion_msg.twist.covariance[6] = sensor_motion_covariance_matrix(1, 0);
  sensor_motion_msg.twist.covariance[7] = sensor_motion_covariance_matrix(1, 1);
  sensor_motion_msg.twist.covariance[11] = sensor_motion_covariance_matrix(1, 2);
  sensor_motion_msg.twist.covariance[30] = sensor_motion_covariance_matrix(2, 0);
  sensor_motion_msg.twist.covariance[31] = sensor_motion_covariance_matrix(2, 1);
  sensor_motion_msg.twist.covariance[35] = sensor_motion_covariance_matrix(2, 2);

  egomotion_publisher_.publish(sensor_motion_msg);
}
