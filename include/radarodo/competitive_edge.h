/********************************COPYRIGHT CHRIS D. MONACO, 2019**********************************/
#pragma once

#include <opencv2/core/core.hpp>
#include <g2o/core/base_unary_edge.h>
#include <eigen3/Eigen/Core>

#include <radarodo/heading_change_vertex.h>

namespace Radarodo {
/**
 * @brief Class describing the edge constraining the sensor's HeadingChangeVertex pose node
 *        using previously extracted pixel points and the current polar RADAR image to
 *        to calculate the sensor's heading change. Error metric based on points "competing"
 *        for higher SNR regions.
 */
class CompetitiveEdge : public g2o::BaseUnaryEdge<1, cv::Point2d, HeadingChangeVertex> {
 public:
  /**
   * @brief Construct a new CompetitiveEdge object
   * 
   * @param[in] polar_image                     CV_16UC1 RADAR polar image that has been blurred
   *                                              to account for trans. vel. estimate uncertainty
   * @param[in] dI_dtheta                       CV_64F image representing the partial derivative of
   *                                              polar image intensity with respect to theta
   * @param[in] previous_extracted_pt           Pixel coordinate of extracted pixel point
   * @param[in] previous_extracted_pt_weight    Weight of extracted pixel point from previous RADAR
   *                                              image (fraction of its image max)
   * @param[in] radians_per_pixel               RADAR polar image radians per pixel [rad/px]
   * @param[in] polar_image_max                 Max intensity observed in current RADAR image
   * @param[in] vertex_ptr                      Pointer of connected pose node
   */
  CompetitiveEdge(
    const cv::Mat& polar_image,
    const cv::Mat& dI_dtheta,
    const cv::Point2d& previous_extracted_pt,
    const double previous_extracted_pt_weight,
    const double radians_per_pixel,
    const double polar_image_max,
    g2o::OptimizableGraph::Vertex* const vertex_ptr)
    : polar_image_(polar_image),
      dI_dtheta_(dI_dtheta),
      radians_per_pixel_(radians_per_pixel),
      polar_image_max_(polar_image_max),
      transformed_pt_(previous_extracted_pt),
      transformed_pt_rounded_(previous_extracted_pt) {
    // If pixel point y (v-coordinate) is within image bounds, measurement is valid
    // (y-coordinate in polar image never changes when optimizing heading)
    if (transformed_pt_rounded_.y >= 0 && transformed_pt_rounded_.y < polar_image_.rows) {
      _measurement = previous_extracted_pt;

      // Sets edge information (weight) to the extracted pixel point's weight from the previous
      // RADAR image. Thus, points from a higher SNR have a higher influence on the pose estimate.
      _information[0] = previous_extracted_pt_weight;

      setVertex(0, vertex_ptr);
    }
  }

  // Compute point's error as its projected pixel intensity's deviation from the current polar
  // image's max (range is 0.0 - 1.0). A point's error is minimized if it projects to a pixel
  // with a higher SNR-based intensity.
  void computeError() final {
    // Calculate new point image coordinates after they were transformed according to the
    // latest sensor motion estimate
    CalculateTransformedPoint();

    // Only calculate/set the error if the point projects within image bounds,
    // otherwise, set to zero.
    _error[0] = InImage() ? 1.0 - ProjectedIntensity() / polar_image_max_ : 0.0;
  }

  // Compute point's error jacobian with respect to the sensor heading change
  void linearizeOplus() final {
    // Calculate new point image coordinates after they were transformed according to the
    // latest sensor motion estimate
    CalculateTransformedPoint();

    // Only calculate/set the jacobian if the point projects within image bounds,
    // otherwise, set to zero.
    _jacobianOplusXi[0] = InImage() ? ProjectedJacobian() / polar_image_max_ : 0.0;
  }

  bool read(std::istream& is) override { return is.good(); }
  bool write(std::ostream& os) const override { return os.good(); }

 private:
  // Returns true if the x-coord. (u-coord.) of the pixel point is within image bounds. Only
  // checks x because y-coord. doesn't change during optimization and it was already validated
  // upon initialization
  inline bool InImage() {
    return (transformed_pt_rounded_.x >= 0 && transformed_pt_rounded_.x < polar_image_.cols);
  }

  // Calculates the new point image coordinates after they wwere transformed according to the
  // latest sensor motion estimate.
  inline void CalculateTransformedPoint() {
    // Calculates floating precision point
    transformed_pt_.x = _measurement.x
                        + static_cast<const HeadingChangeVertex*>(_vertices[0])->estimate() /
                            radians_per_pixel_;

    // Calculates rounded int point
    transformed_pt_rounded_.x = std::round(transformed_pt_.x);
  }

  // Get partial derivative of polar image intensity with respect to theta by looking up
  // pixel intensity values stored in dI_dtheta_ image at the transformed point location
  inline double ProjectedJacobian() {
    return static_cast<double>(dI_dtheta_.at<double>(transformed_pt_rounded_));
  }

  // Get SNR-based projected intensity of RADAR polar image at the transformed point location
  inline double ProjectedIntensity() {
    // Uses partial derivative to calculate projected intensity to mitigate discretiztion caused
    // by image pixel lookup. This instead emulates a piecewise function that depends
    // on the point's deviation from the pixel center. This is calculated by using the difference
    // between the floating precision coordinate and its rounded coordinate.
    return static_cast<double>(polar_image_.at<ushort>(transformed_pt_rounded_))
           + ProjectedJacobian() * (transformed_pt_rounded_.x - transformed_pt_.x) *
               radians_per_pixel_;
  }

  const cv::Mat& polar_image_;            // CV_16UC1 RADAR polar image that has been blurred
                                          //   to account for trans. vel. estimate uncertainty
  const cv::Mat& dI_dtheta_;              // CV_64F image representing the partial derivative of
                                          //   polar image intensity with respect to theta
  const double radians_per_pixel_;        // Polar RADAR image radians per pixel [rad./px.]
  const double polar_image_max_;          // Maximum pixel intensity observed in RADAR image
  cv::Point2d transformed_pt_;            // Transformed pixel point coordinates [double]
  cv::Point2i transformed_pt_rounded_;    // Rounded transformed pixel point coordinates [int]
};

}  // namespace Radarodo
