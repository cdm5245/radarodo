/********************************COPYRIGHT CHRIS D. MONACO, 2019**********************************/
#pragma once

#include <g2o/core/base_vertex.h>

namespace Radarodo {
/**
 * @brief Class that represents the sensor's heading change via a g2o pose vertex
 * 
 */
class HeadingChangeVertex : public g2o::BaseVertex<1, double> {
 public:
  void setToOriginImpl() { _estimate = 0; }

  // Heading change updates are simply additive since they should not exceed
  // a magnitude of 2 PI
  void oplusImpl(const number_t* update) { _estimate += *update; }

  bool read(std::istream& is) override { return is.good(); }
  bool write(std::ostream& os) const override { return os.good(); }
};

}  // namespace Radarodo
