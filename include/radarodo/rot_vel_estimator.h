/********************************COPYRIGHT CHRIS D. MONACO, 2019**********************************/
#pragma once

#include <vector>

#include <opencv2/core/core.hpp>
#include <eigen3/Eigen/Core>
#include <g2o/core/sparse_optimizer.h>

/**
 * @brief Class for estimating sensor rotational motion (yaw rate) from RADAR images
 * 
 */
class RotVelEstimator {
 public:
  /**
   * @brief Construct a new RotVelEstimator object
   * 
   * @param[in] radians_per_pixel           RADAR polar image radians per pixel [rad/px.]
   * @param[in] extracted_points_weight_fraction_threshold 
   *                                        minimum weight threshold for point extraction
   *                                          (defined by fraction of image's max)
   * @param[in] max_num_extracted_points    Maximum number of extracted pixel points
   * @param[in] max_num_iterations          Maximum number of iterations for non-linear 
   *                                          optimization (Default: 10)
   * @param[in] verbose                     Flag if optimization process should be verbose
   *                                          (Default: false)
   */
  RotVelEstimator(const double radians_per_pixel,
                  const double extracted_points_weight_fraction_threshold,
                  const uint max_num_extracted_points,
                  const uint max_num_iterations = 10,
                  const bool verbose = false);

  /**
   * @brief Estimate sensor yaw rate from polar RADAR image
   * 
   * @param[in] blurred_polar_image                     CV_16UC1 polar RADAR image blurred according
   *                                                      to translational velocity uncertainty
   * @param[in] delta_t                                 Time between RADAR image measurements [sec]
   * @param[in,out] sensor_motion_covariance_matrix     3x3 covariance matrix that corresponds to 
   *                                                      the uncertainty of the 3x1 motion estimate 
   * @return double                                     yaw-rate estimate [rad/s]
   */
  double Estimate(const cv::Mat& blurred_polar_image,
                  const double delta_t,
                  Eigen::Matrix<double, 3, 3>& sensor_motion_covariance_matrix);

  /**
   * @brief Returns flag if points from the previous RADAR image were stored
   * 
   * @return true     If points from the previous RADAR image were stored
   * @return false    Otherwise
   */
  inline bool previous_points_stored() const { return previous_points_stored_; }

  /**
   * @brief Returns the extracted pixel point data (coordinates and weights) for visual debugging
   * 
   * @return const Eigen::Array<double, Eigen::Dynamic, 3>&   Nx3 array of extracted pixel point
   *                                                          data (u-coord., v-coord., weight)
   */
  inline const Eigen::Array<double, Eigen::Dynamic, 3>& extracted_point_data() const {
    return extracted_point_data_;
  }

  /**
   * @brief Translates extracted pixel points from the previous polar RADAR image
   * 
   * @param[in] polar_pixel_translations    Nx2 array of polar pixel translations
   *                                          (u-coord., v-coord.) corresponding to the current
   *                                          translational velocity estimate
   */
  inline void TranslatePreviousPoints(
      const Eigen::Array<double, Eigen::Dynamic, 2>& polar_pixel_translations) {
    extracted_point_data_.leftCols<2>() += polar_pixel_translations;
  }

  /**
   * @brief Stores extracted pixel points from the RADAR polar image for use on the next image
   * 
   * @param[in] polar_image     CV_16UC1 polar RADAR image
   */
  void StoreMeasurement(const cv::Mat& polar_image);

 private:
  /**
  * @brief Generates the "skeleton" framework for the g2o pose graph (generates pose node
  *        and prior edge)
  */
  void GenerateSkeletonFramework();

  /**
   * @brief Returns the maximum pixel intensity observed in the image
   * 
   * @param[in] image     Image
   * @return double       Maximum pixel intensity observed in the image 
   */
  inline double GetImageMax(const cv::Mat& image) {
    double max;
    cv::minMaxLoc(image, nullptr, &max);
    return max;
  }

  /**
   * @brief 
   * 
   * @param[in] extracted_point_indices 
   * @param[in] polar_image 
   * @param[in] num_extracted_points 
   * @param[in] polar_image_max 
   */
  void RandomlySamplePoints(const std::vector<cv::Point>& extracted_point_indices,
                            const cv::Mat& polar_image,
                            const uint num_extracted_points,
                            const double polar_image_max);

  /**
   * @brief Generate the pose graph (via g2o) that sets up the non-linear optimization
   *        framework to estimate the sensor's heading change
   * 
   * @param[in] blurred_polar_image     CV_16UC1 RADAR polar image blurred to account for 
   *                                      translational velocity uncertainty
   * @param[in] dI_dtheta               CV_64F image that contains values of the partial derivative
   *                                      of pixel intensity with respect to theta [px./rad]
   */
  void GeneratePoseGraph(const cv::Mat& blurred_polar_image,
                         const cv::Mat& dI_dtheta);

  const double radians_per_pixel_;                            // RADAR polar image radians per
                                                              //   pixel [rad/px.]
  const double extracted_points_weight_fraction_threshold_;   // Minimum weight threshold for point
                                                              //   extraction (defined by fraction
                                                              //   of image's max)
  const uint max_num_extracted_points_;                       // Maximum number of extracted pixel
                                                              //  points
  const uint max_num_iterations_;                             // Maximum number of iterations for
                                                              //   non-linear optimization
  bool previous_points_stored_;                               // Flag denoting if previous image's
                                                              //   points were stored
  const cv::Mat sobel_kernel_dI_dtheta_;                      // CV_64F Sobel derivative kernel
                                                              //   used on the polar RADAR image to
                                                              //   calculate dI_dtheta
  g2o::SparseOptimizer optimizer_pose_graph_;                 // g2o non-linear optimization pose
                                                              //   graph
  Eigen::Array<double, Eigen::Dynamic, 3> extracted_point_data_;
                                                              // Nx3 array of extracted pixel point
                                                              //   data (u-coord., v-coord., weight)
};
