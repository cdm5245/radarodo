#pragma once

/********************************COPYRIGHT CHRIS D. MONACO, 2019**********************************/
#include <string>

#include <opencv2/core/core.hpp>
#include <eigen3/Eigen/Core>

/**
 * @brief Namespace for functions that support visual (image-based) debugging
 * 
 */
namespace VisualDebugger {

void GenerateColormapImage(const cv::Mat& image, cv::Mat& colormap_image);
void WriteImageToDisk(const std::string& package_path,
                      const uint msg_seq,
                      const cv::Mat& image, 
                      const std::string& prefix);
void RecordDebuggingImages(const std::string& package_path,
                           const uint msg_seq,
                           const cv::Mat& blurred_polar_image,
                           const Eigen::Vector3d& sensor_motion_estimate, 
                           const Eigen::Array<double, Eigen::Dynamic, 3>& extracted_point_data,
                           const double gps_yaw_rate,
                           const double delta_t,
                           const double radians_per_pixel);
void AnnotateImageWithPoints(const Eigen::Array<double, Eigen::Dynamic, 3>& extracted_point_data,
                             const double u_pixel_offset,
                             cv::Mat& polar_image);
double CalculateError(const Eigen::Array<double, Eigen::Dynamic, 3>& extracted_point_data,
                      const double u_pixel_offset,
                      const cv::Mat& polar_image,
                      const bool weighted = false);
void AnnotateImageWithInfo(const double initial_error,
                           const double weighted_initial_error,
                           const double radarodo_error,
                           const double weighted_radarodo_error,
                           const double gps_error,
                           const double weighted_gps_error,
                           const double radarodo_yaw_rate,
                           const double gps_yaw_rate,
                           const double delta_t,
                           const double radians_per_pixel,
                           cv::Mat& pre_rot_image,
                           cv::Mat& post_rot_image,
                           cv::Mat& gps_rot_image);
void RecordPolarAndCartesianImages(const std::string& package_path,
                                   const uint msg_seq,
                                   const cv::Mat& polar_image,
                                   const cv::Mat& cartesian_image,
                                   const cv::Mat& polar_non_field_of_view_mask,
                                   const cv::Mat& cartesian_non_field_of_view_mask);
void RecordExtractedPointImages(
    const std::string& package_path,
    const uint msg_seq,
    const cv::Mat& polar_image,
    const Eigen::Array<double, Eigen::Dynamic, 3>& extracted_point_data,
    const cv::Mat& polar_non_field_of_view_mask,
    const double u_pixel_offset,
    const std::string& prefix);

}  // namespace VisualDebugger