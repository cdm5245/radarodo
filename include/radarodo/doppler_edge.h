/********************************COPYRIGHT CHRIS D. MONACO, 2019**********************************/
#pragma once

#include <cmath>

#include <eigen3/Eigen/Core>
#include <g2o/core/base_unary_edge.h>
#include <g2o/core/optimizable_graph.h>
#include <g2o/types/slam2d/vertex_point_xy.h>

namespace Radarodo {

/**
 * @brief Class describing the edge (based on RADAR Doppler measurements) constraining 
 *        the sensor's translational velocity estimate (represented with a g2o VertexPointXY
 *        pose node).
 * 
 */
class DopplerEdge : public g2o::BaseUnaryEdge<2, Eigen::Vector2d, g2o::VertexPointXY> {
 public:
   /**
    * @brief Construct a new DopplerEdge object from RADAR target measurement
    * 
    * @param[in] range_rate                       Target range rate [m/s]
    * @param[in] theta_rad                        Target azimuthal angle [rad]
    * @param[in] cos_theta                        Cosine of measurement azimuthal angle
    * @param[in] sin_theta                        Sine of measurement azimuthal angle
    * @param[in] measurement_covariance_matrix    2x2 covariance matrix describing the uncertainty
    *                                               of a RADAR target measurement (First row/col: 
    *                                               range rate, second row/col: azimuthal angle).
    * @param[in] vertex_ptr                       Pointer of pose node vertex
    */
  DopplerEdge(const double range_rate,
              const double theta_rad,
              const double cos_theta,
              const double sin_theta,
              const Eigen::Matrix2d& measurement_covariance_matrix,
              g2o::OptimizableGraph::Vertex* const vertex_ptr)
  : cos_theta_(cos_theta),
    sin_theta_(sin_theta) {
    setVertex(0, vertex_ptr);
    _measurement << range_rate, theta_rad;

    // Calculate the partial derivative of the v_x and v_y sensor estimates with respect
    // to the measurement's range rate and azimuthal angle
    Eigen::Matrix<double, 2, 2> partial_vxy_partial_measurement;
    partial_vxy_partial_measurement <<
      partial_vx_partial_rdot(), partial_vx_partial_theta(),
      partial_vy_partial_rdot(), partial_vy_partial_theta();

    // Use the partial derivatives to tranform the RADAR measurement's covariance matrix
    // to the trans. vel. estimate covariance matrix
    const Eigen::Matrix<double, 2, 2> error_covariance_matrix =
      partial_vxy_partial_measurement *
      measurement_covariance_matrix *
      partial_vxy_partial_measurement.transpose();

    // Set the edge's information matrix to the inverse of its calculated covariance matrix
    setInformation(error_covariance_matrix.inverse());
  }

  void computeError() final {
    // Updates stored calculations in cache based on new pose node estimate
    UpdateCacheForErrorCalc();

    // Calculates error (L1 difference from predicted range rate and azimuthal angle)
    // Error function used as predicted thetas at low speeds are wildly unreliable
    _error << rdot() - rdot_pred(),
              (theta() - theta_pred()) * std::erf(cache_.v);
  }

  void linearizeOplus() final {
    // Updates stored calculations in cache based on new pose node estimate
    UpdateCacheForJacobianCalc();

    // Calculates jacobian of range rate and theta errors with respect to v_x and v_y
    _jacobianOplusXi << - partial_rdot_pred_partial_vx(), - partial_rdot_pred_partial_vy(),
                        - partial_theta_pred_partial_vx(), - partial_theta_pred_partial_vy();
  }

 private:
  // Sensor longitudinal velocity estimate
  inline double v_x() {
    return static_cast<const g2o::VertexPointXY*>(_vertices[0])->estimate()[0];
  }

  // Sensor lateral velocity estimate
  inline double v_y() {
    return static_cast<const g2o::VertexPointXY*>(_vertices[0])->estimate()[1];
  }

  // Target measurement's range rate [m/s]
  inline double rdot() {
    return _measurement[0];
  }

  // Target measurement's azimuthal angle [rad]
  inline double theta() {
    return _measurement[1];
  }

  // Partial derivative of long. vel. w.r.t. range rate
  inline double partial_vx_partial_rdot() {
    return - 1.0 / cos_theta_;
  }

  // Partial derivative of lat. vel. w.r.t. range rate
  inline double partial_vy_partial_rdot() {
    return - 1.0 / sin_theta_;
  }

  // Partial derivative of long. vel. w.r.t. azimuthal angle
  inline double partial_vx_partial_theta() {
    return (- sin_theta_ * rdot() - v_y()) / (cos_theta_ * cos_theta_);
  }

  // Partial derivative of lat. vel. w.r.t. azimuthal angle
  inline double partial_vy_partial_theta() {
    return (cos_theta_ * rdot() - v_x()) / (sin_theta_ * sin_theta_);
  }

  // The negative of the ratio of range rate to the sensor trans. vel. estimate's Euclidean norm
  inline double neg_ratio_rdot_to_v() {
    return std::abs(rdot()) > cache_.v ? std::copysign(1.0, -rdot()) : -rdot()/cache_.v;
  }

  // Updates stored calculations in cache based on new pose node estimate for error calculation
  inline void UpdateCacheForErrorCalc() {
    cache_.v = static_cast<const g2o::VertexPointXY*>(_vertices[0])->estimate().norm();
    cache_.neg_ratio_rdot_to_v = neg_ratio_rdot_to_v();
  }

  // Predicted range rate based on sensor velocity estimate
  inline double rdot_pred() {
    return - cos_theta_ * v_x() - sin_theta_ * v_y();
  }

  // Azimuthal angle describing direction of sensor's translational motion [rad]
  inline double alpha() {
    return std::atan2(v_y(), v_x());
  }

  // Azimuthal angle describing the deviation of the target's azimuthal angle from the
  // calculated alpha angle [rad]. Only positive values returned.
  inline double beta() {
    return std::acos(cache_.neg_ratio_rdot_to_v);
  }

  // Predicted azimuthal angle of the target, given range rate and sensor motion estimates.
  // The predicted theta actually depends on the actual measured theta because each range rate
  // could have plausibly originated from two azimuthal angles. The alpha and theta comparison
  // ensures that the calculated predicted theta is the one closest to the actual theta.
  inline double theta_pred() {
    return alpha() < theta() ? alpha() + beta() : alpha() - beta();
  }

  // Updates stored calculations in cache based on new pose node estimate for jacobian calc.
  inline void UpdateCacheForJacobianCalc() {
    cache_.v = static_cast<const g2o::VertexPointXY*>(_vertices[0])->estimate().norm();
    cache_.v2 = cache_.v * cache_.v;
    cache_.v3 = cache_.v2 * cache_.v;
    cache_.neg_ratio_rdot_to_v = neg_ratio_rdot_to_v();
    cache_.ratio_rdot2_to_v2 = cache_.neg_ratio_rdot_to_v * cache_.neg_ratio_rdot_to_v;
    cache_.sqrt_one_minus_ratio_rdot2_to_v2 = std::sqrt(1.0 - cache_.ratio_rdot2_to_v2);

    // Range rate is saturated if the range rate measurment is implausible given the sensor
    // motion estimate (range rate magnitude is larger than motion estimate norm)
    cache_.range_rate_saturated = cache_.ratio_rdot2_to_v2 == 1 ? true : false;
  }

  // Partial derivative of predicted range rate w.r.t. long. vel.
  inline double partial_rdot_pred_partial_vx() {
    return - cos_theta_;
  }

  // Partial derivative of predicted range rate w.r.t. lat. vel.
  inline double partial_rdot_pred_partial_vy() {
    return - sin_theta_;
  }

  // Partial derivative of alpha w.r.t. long. vel.
  inline double partial_alpha_partial_vx() {
    return - v_y() / cache_.v2;
  }

  // Partial derivative of alpha w.r.t. lat. vel.
  inline double partial_alpha_partial_vy() {
    return v_x() / cache_.v2;
  }

  // Partial derivative of beta w.r.t. long. vel.
  inline double partial_beta_partial_vx() {
    return cache_.range_rate_saturated ?
           0 : (- rdot() * v_x()) / (cache_.v3 * cache_.sqrt_one_minus_ratio_rdot2_to_v2);
  }

  // Partial derivative of beta w.r.t. lat. vel.
  inline double partial_beta_partial_vy() {
    return cache_.range_rate_saturated ?
           0 : (- rdot() * v_y()) / (cache_.v3 * cache_.sqrt_one_minus_ratio_rdot2_to_v2);
  }

  // Partial derivative of predicted theta w.r.t. long. vel. Depends on theta because
  // two plausible predicted theta values are possible. The comparison ensures the partial
  // derivative of the predicted theta closest to the actual theta measurement.
  inline double partial_theta_pred_partial_vx() {
    return alpha() < theta() ?
           partial_alpha_partial_vx() + partial_beta_partial_vx() :
           partial_alpha_partial_vx() - partial_beta_partial_vx();
  }

  // Partial derivative of predicted theta w.r.t. lat. vel. Depends on theta because
  // two plausible predicted theta values are possible. The comparison ensures the partial
  // derivative of the predicted theta closest to the actual theta measurement.
  inline double partial_theta_pred_partial_vy() {
    return alpha() < theta() ?
           partial_alpha_partial_vy() + partial_beta_partial_vy() :
           partial_alpha_partial_vy() - partial_beta_partial_vy();
  }

  bool read(std::istream& is) override { return is.good(); }
  bool write(std::ostream& os) const override { return os.good(); }

  const double cos_theta_;    // Cosine of measured target azimuthal angle
  const double sin_theta_;    // Sine of measured target azimuthal angle

  /**
   * @brief Cache class to store values that are recalculated multiple times per iteration 
   *        (for efficiency)
   * 
   */
  struct Cache {
    double v;                                   // Euclidean norm of sensor trans. vel. estimate
    double v2;                                  // Velocity norm squared
    double v3;                                  // Velocity norm to the third power
    double neg_ratio_rdot_to_v;                 // Negative ratio of range rate measurement to
                                                //   euclidean norm of sensor trans. vel. estimate
    double ratio_rdot2_to_v2;                   // Squared ratio of range rate to sensor velocity
                                                //   norm
    double sqrt_one_minus_ratio_rdot2_to_v2;    // Square root of one minus the squared ratio of
                                                //   of range rate to sensor velocity norm
    bool range_rate_saturated;                  // Flag that marks if the measured rate rate is
                                                //   implausible given sensor motion estimate
                                                //   (magnitude larger than velocity norm)
  };
  Cache cache_;                                 // Cache for repeated calculated values
};

}  // namespace Radarodo

