/********************************COPYRIGHT CHRIS D. MONACO, 2019**********************************/
#pragma once

#include <vector>

#include <eigen3/Eigen/Core>
#include <eigen3/Eigen/Dense>
#include <g2o/core/sparse_optimizer.h>
#include <g2o/types/slam2d/vertex_point_xy.h>

#include <radarodo/radar_detection.h>

/**
 * @brief Class that estimates a sensor's translational velocities from RADAR Doppler data
 * 
 */
class TransVelEstimator {
 public:
  /**
   * @brief Construct a new TransVelEstimator object
   * 
   * @param[in] ransac_iterations                 Number of RANSAC iterations for outlier rejection
   * @param[in] range_rate_inlier_threshold       Range rate residual threshold for a 
   *                                                measurement to be considered an inlier
   *                                                in the RANSAC scheme
   * @param[in] target_range_rate_sigma           Expected standard deviation of the RADAR's
   *                                                range rate measurements [m/s]
   * @param[in] target_theta_sigma                Expected standard deviation of the RADAR's
   *                                                returned targets' azimuthal angles [rad/s]
   * @param[in] max_num_optimization_iterations   Maximum number of iterations for the 
   *                                                estimation's non-linear optimization
   * @param[in] verbose                           If true, designates whether to output 
   *                                                optimization details to the terminal while 
   *                                                optimizing
   */
  TransVelEstimator(const double ransac_iterations,
                    const double range_rate_inlier_threshold,
                    const double target_range_rate_sigma,
                    const double target_theta_sigma,
                    const uint max_num_optimization_iterations = 10,
                    const bool verbose = false);

  /**
   * @brief Estimate the sensor's translational velocities via Doppler measurements
   * 
   * @param[in,out] valid_radar_detections          Vector of RadarDetection objects describing
   *                                                  valid targets
   * @param[out] sensor_motion_estimate             3x1 vector of sensor motion estimate
   *                                                  [v_x v_y omega_z] (m/s, m/s, rad/s)
   * @param[out] sensor_motion_covariance_matrix    3x3 sensor motion covariance matrix corr.
   *                                                  to sensor_motion_estimate
   * @return true                                   If valid trans. vel. calculated
   * @return false                                  Otherwise
   */
  bool Estimate(std::vector<RadarDetection>& valid_radar_detections,
                Eigen::Vector3d& sensor_motion_estimate,
                Eigen::Matrix<double, 3, 3>& sensor_motion_covariance_matrix);

 private:
  /**
   * @brief Generate matrices that describe the linear system relating range rate measurements
   *        to the sensor's translational velocities
   * 
   * @param[in] valid_radar_detections    Vector of RadarDetection objects describing valid targets
   * @param[out] A                        Mx2 matrix that represents the linear system (based on 
   *                                        azimuthal angles). M is the number of detected targets.
   * @param[out] b                        Mx1 vector the holds the corresponding range rates [m/s]
   * @param[out] ranges                   Mx1 vector that holds the corresponding ranges [m]
   * @param[out] thetas                   Mx1 vector that holds the corresponding azimuthal angles
   *                                        [rad]
   */
  void GenerateMatrices(const std::vector<RadarDetection>& valid_radar_detections,
                        Eigen::Matrix<double, Eigen::Dynamic, 2>& A,
                        Eigen::Matrix<double, Eigen::Dynamic, 1>& b,
                        Eigen::Matrix<double, Eigen::Dynamic, 1>& ranges,
                        Eigen::Matrix<double, Eigen::Dynamic, 1>& thetas);

  /**
   * @brief Extract (static) inliers from all valid RADR targets using their 
   *        cohesive range rate measurements via RANSAC
   * 
   * @param[in] A                              Mx2 matrix that represents the linear system (based
   *                                             on azimuthal angles). M is the number of targets.
   * @param[in] b                              Mx1 vector the holds the corresponding range rates 
   *                                             [m/s]
   * @param[out] inlier_flags                  Mx1 [uint] vector that flags if a target is an
   *                                             inlier (1 = inlier, 0 = outlier)
   * @param[in, out] valid_radar_detections    Vector of RadarDetection objects describing valid 
   *                                             targets
   * @return uint                              Number of detected inliers (N)
   */
  uint ExtractInliers(const Eigen::Matrix<double, Eigen::Dynamic, 2>& A,
                      const Eigen::Matrix<double, Eigen::Dynamic, 1>& b,
                      Eigen::Array<uint, Eigen::Dynamic, 1>& inlier_flags,
                      std::vector<RadarDetection>& valid_radar_detections);

  /**
   * @brief Generates the necessary matrices/vectors, but only including the rows
   *        corresponding to inlier targets
   * 
   * @param[in] A                  Mx2 matrix that represents the linear system (based
   *                                 on azimuthal angles). M is the number of targets.
   * @param[in] b                  Mx1 vector the holds the corresponding range rates 
   *                                 [m/s]
   * @param[in] ranges             Mx1 vector that holds the corresponding ranges [m]
   * @param[in] thetas             Mx1 vector that holds the corresponding azimuthal angles
   *                                 [rad]
   * @param[in] inlier_flags       Mx1 [uint] vector that flags if a target is an
   *                                 inlier (1 = inlier, 0 = outlier)
   * @param[in] num_inliers        Number of detected inliers (N)
   * @param[out] A_inliers         Nx2 matrix that represents the linear system for inliers
   *                                 only. N is the number of inlier targets.
   * @param[out] b_inliers         Nx1 vector that holds inliers' range rates [m/s]
   * @param[out] range_inliers     Nx1 vector that holds inliers' ranges [m]
   * @param[out] theta_inliers     Nx1 vector that holds inliers' azimuthal angles [rad]
   */
  void GenerateInlierMatrices(const Eigen::Matrix<double, Eigen::Dynamic, 2>& A,
                              const Eigen::Matrix<double, Eigen::Dynamic, 1>& b,
                              const Eigen::Matrix<double, Eigen::Dynamic, 1>& ranges,
                              const Eigen::Matrix<double, Eigen::Dynamic, 1>& thetas,
                              const Eigen::Array<uint, Eigen::Dynamic, 1>& inlier_flags,
                              const uint num_inliers,
                              Eigen::Matrix<double, Eigen::Dynamic, 2>& A_inliers,
                              Eigen::Matrix<double, Eigen::Dynamic, 1>& b_inliers,
                              Eigen::Matrix<double, Eigen::Dynamic, 1>& range_inliers,
                              Eigen::Matrix<double, Eigen::Dynamic, 1>& theta_inliers);

  /**
   * @brief Estimates the sensor's translational velocities via Least Squares
   * 
   * @param[in] A_inliers       Nx2 matrix that represents the linear system for inliers
   *                              only. N is the number of inlier targets.
   * @param[in] b_inliers       Nx1 vector that holds inliers' range rates [m/s]
   * @return Eigen::Vector2d    2x1 vector of sensor trans. vel. [v_x v_y] (m/s, m/s)
   */
  inline Eigen::Vector2d EstimateLeastSquaresSolution(
      const Eigen::Matrix<double, Eigen::Dynamic, 2>& A_inliers,
      const Eigen::Matrix<double, Eigen::Dynamic, 1>& b_inliers) {
    // Calculate least squares solution of Ax=b, but using the psuedo-inverse
    return (A_inliers.transpose() * A_inliers).ldlt().solve(A_inliers.transpose() * b_inliers);
  }


  /**
   * @brief Generate the pose graph (via g2o) that sets up the non-linear optimization
   *        framework to further refine the trans. vel. estimate
   * 
   * @param[in] A_inliers                       Nx2 matrix that represents the linear system for
   *                                              inliers only. N is the number of inlier targets.
   * @param[in] b_inliers                       Nx1 vector that holds inliers' range rates [m/s]
   * @param[in] theta_inliers                   Nx1 vector that holds inliers' azimuthal angles 
   *                                              [rad]
   * @param[in] sensor_trans_motion_estimate    2x1 vector of sensor trans. vel. [v_x v_y]
   *                                              (m/s, m/s)
   */
  void GeneratePoseGraph(const Eigen::Matrix<double, Eigen::Dynamic, 2>& A_inliers,
                         const Eigen::Matrix<double, Eigen::Dynamic, 1>& b_inliers,
                         const Eigen::Matrix<double, Eigen::Dynamic, 1>& theta_inliers,
                         const Eigen::Vector2d& sensor_trans_motion_estimate);

  /**
   * @brief Generates the "skeleton" framework for the g2o pose graph (generates pose node
   *        and prior edge)
   * 
   * @param[in] sensor_trans_motion_estimate    2x1 vector of sensor trans. vel. [v_x v_y]
   *                                              (m/s, m/s)
   */
  void GenerateSkeletonFramework(const Eigen::Vector2d& sensor_trans_motion_estimate);

  /**
   * @brief Calculate refined pose estimate via non-linear optimization
   * 
   * @param[in] pose_vertex_id    Vertex id of pose node [uint]
   * @return Eigen::Vector2d      Refined 2x1 vector of sensor trans. vel. [v_x v_y]
   *                                (m/s, m/s)
   */
  inline Eigen::Vector2d PoseEstimate(const uint pose_vertex_id) {
    return static_cast<g2o::VertexPointXY*>(
      optimizer_pose_graph_.vertex(pose_vertex_id))->estimate();
  }

  /**
   * @brief Calculate the trans. vel. estimate covariance matrix 
   * 
   * @param[in] A_inliers                           Nx2 matrix that represents the linear system
   *                                                  for inliers only. N is the number of inlier
   *                                                  targets.
   * @param[in] range_inliers                       Nx1 vector that holds inliers' ranges [m]
   * @param[out] sensor_motion_covariance_matrix    3x3 covariance matrix that corresponds to the
   *                                                uncertainty of the 3x1 sensor_motion_estimate
   */
  void CalculateCovarianceMatrix(const Eigen::Matrix<double, Eigen::Dynamic, 2>& A_inliers,
                                 const Eigen::Matrix<double, Eigen::Dynamic, 1>& range_inliers,
                                 Eigen::Matrix<double, 3, 3>& sensor_motion_covariance_matrix);

  const double ransac_iterations_;                  // Number of iterations for RANSAC inlier
                                                    //   extraction
  const double range_rate_inlier_threshold_;        // Range rate residual threshold for a
                                                    //   measurement to be considered an inlier
  const double max_num_optimization_iterations_;    // Maximum number of iterations for non-linear
                                                    //   optimization
  const Eigen::Matrix<double, 2, 2> measurement_covariance_matrix_;
        // 2x2 covariance matrix describing the uncertainty of the RADAR target measurements
        // (First row/col. is range rate, second row/col. is azimuthal angle)
  g2o::SparseOptimizer optimizer_pose_graph_;       // g2o optimizer pose graph
};
