/********************************COPYRIGHT CHRIS D. MONACO, 2019**********************************/
#pragma once

#include <string>
#include <fstream>

#include <ros/ros.h>
#include <geometry_msgs/TwistStamped.h>
#include <geometry_msgs/TwistWithCovarianceStamped.h>

/**
 * @brief Class to analyze RADARODO's estimation results (errors) and computational performance
 * 
 */
class Analyzer {
 public:
  /**
   * @brief Construct a new Analyzer object
   * 
   * @param[in] package_path      RADARODO ROS package path
   * @param[in] record_results    Flag to determine if results (errors) should be recorded
   *                                (Default: false)
   * @param[in] record_runtimes   Flag to determine if runtimes should be recorded
   *                                (Default: false)
   */
  Analyzer(const std::string& package_path,
           const bool record_results = false,
           const bool record_runtimes = false);

  /**
   * @brief Record RADARODO estimation results
   * 
   */
  void RecordResults();

  /**
   * @brief Record RADARODO sub-processes runtimes
   * 
   * @param[in] trans_vel_estimation_us             Trans. vel. estimation runtime [microsec]
   * @param[in] radar_image_generation_us           RADAR image generation runtime [microsec]
   * @param[in] remapping_us                        Remapping transformations runtime [microsec]
   * @param[in] rot_vel_estimation_us               Rot. Vel. estimation runtime [microsec]
   * @param[in] point_extraction_preparation_us     Point extraction + prep. runtime [microsec]
   */
  void RecordRuntimes(const int trans_vel_estimation_us,
                      const int radar_image_generation_us,
                      const int remapping_us,
                      const int rot_vel_estimation_us,
                      const int point_extraction_preparation_us);

  /**
   * @brief Returns yaw rate estimate as calculated from time derivative of GNSS Doppler 
   *        measurements
   * 
   * @return double     GPS Doppler-based yaw rate estimate [rad/s]
   */
  inline double gps_yaw_rate() {return gps_.yaw_rate; }

 private:
  /**
   * @brief Callback for GPS-based motion estimate ROS messages
   * 
   * @param[in] gps_motion_msg_ptr    GPS-based motion estimate ROS message
   */
  void GpsMotionCallback(
    const geometry_msgs::TwistStampedConstPtr gps_motion_msg_ptr);

  /**
   * @brief Callback for RADARODO motion estimate ROS messages
   * 
   * @param[in] radarodo_motion_msg_ptr     RADARODO motion estimate ROS message
   */
  void RadarodoMotionCallback(
    const geometry_msgs::TwistWithCovarianceStampedConstPtr radarodo_motion_msg_ptr);

  /**
   * @brief DataSource structure to contain the motion estimates and uncertainties from 
   *        an arbitrary data source
   * 
   */
  struct DataSource {
    ros::Subscriber motion_subscriber_;   // Subscriber to subscribe to the ROS motion messages
    uint seq;                             // ROS message sequence number
    double time;                          // ROS message timestamp time from epoch [sec]
    double vx;                            // Longitudinal trans. velocity estimate [m/s]
    double vy;                            // Lateral trans. velocity estimate [m/s]
    double yaw_rate;                      // Rot. velocity estimate (omega_z) [rad/s]
    double vx_cov;                        // Long. trans. velocity covariance
    double vy_cov;                        // Lat. trans. velocity covariance
    double vxvy_cov;                      // Long./Lat. trans velocity covariance
    double yaw_rate_cov;                  // Yaw rate covariance
  };

  ros::NodeHandle nh_;            // ROS node handle
  DataSource gps_;                // GPS-based data source
  DataSource radarodo_;           // RADARODO data source
  std::ofstream results_csv_;     // File stream to write results to a csv file
  std::ofstream runtimes_csv_;    // File stream to write runtimes to a csv file
};
